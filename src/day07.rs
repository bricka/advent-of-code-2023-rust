use std::{fmt::{Display, Formatter, self}, collections::HashMap, cmp::Ordering, iter::zip};

use crate::solver::Solver;

// Defined in this order, their discriminants are increasing
#[derive(PartialOrd, PartialEq)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(PartialEq, Debug, Clone)]
struct Card {
    representation: char,
    value: usize,
}

impl Card {
    fn from_representation(representation: char, j_is_joker: bool) -> Card {
        let value = match representation {
            'A' => 13,
            'K' => 12,
            'Q' => 11,
            'J' => if j_is_joker { 0 } else { 10 },
            'T' => 9,
            '9' => 8,
            '8' => 7,
            '7' => 6,
            '6' => 5,
            '5' => 4,
            '4' => 3,
            '3' => 2,
            '2' => 1,
            _ => panic!("Unknown card: {}", representation),
        };

        Card {
            representation,
            value,
        }
    }
}

fn cmp_cards(hand1: &[Card], hand2: &[Card]) -> Ordering {
    for (card1, card2) in zip(hand1, hand2) {
        let order = card1.value.cmp(&card2.value);
        if order != Ordering::Equal {
            return order
        }
    }

    Ordering::Equal
}

#[derive(PartialEq, Debug, Clone)]
struct Hand {
    cards: Vec<Card>,
    bid: usize,
}

impl Hand {
    fn get_type(&self) -> HandType {
        let card_counts = self.get_card_counts();
        if card_counts.iter().any(|(_, val)| *val == 5) {
            HandType::FiveOfAKind
        } else if card_counts.iter().any(|(_, val)| *val == 4) {
            HandType::FourOfAKind
        } else if card_counts.iter().any(|(_, val)| *val == 3) && card_counts.iter().any(|(_, val)| *val == 2) {
            HandType::FullHouse
        } else if card_counts.iter().any(|(_, val)| *val == 3) {
            HandType::ThreeOfAKind
        } else if card_counts.iter().filter(|(_, val)| **val == 2).count() == 2 {
            HandType::TwoPair
        } else if card_counts.iter().filter(|(_, val)| **val == 2).count() == 1 {
            HandType::OnePair
        } else {
            HandType::HighCard
        }
    }

    fn get_card_counts(&self) -> HashMap<char, usize> {
        let mut map = HashMap::new();

        for card in &self.cards {
            map.entry(card.representation)
                .and_modify(|count| *count += 1)
                .or_insert(1);
        }

        map
    }

    fn apply_jokers(&self) -> Hand {
        println!("Applying joker to {}", self);
        if !self.has_joker() {
            return self.clone();
        }

        println!("Has joker");

        if self.cards.iter().all(|c| c.representation == 'J') {
            return self.replace_first_joker_with_representation('A').apply_jokers();
        }

        let card_counts = self.get_card_counts();
        let (most_common_card_rep, _) = card_counts.iter().filter(|(rep, _)| **rep != 'J').max_by_key(|(_, val)| **val).unwrap();
        self.replace_first_joker_with_representation(*most_common_card_rep).apply_jokers()
    }

    fn has_joker(&self) -> bool {
        self.cards.iter().any(|c| c.representation == 'J')
    }

    fn replace_first_joker_with_representation(&self, rep: char) -> Hand {
        let mut new_cards = self.cards.clone();
        match new_cards.iter().position(|c| c.representation == 'J') {
            None => panic!("No joker found"),
            Some(joker_index) => {
                new_cards[joker_index].representation = rep;
                Hand {
                    cards: new_cards,
                    bid: self.bid,
                }
            }
        }
    }
}

impl Display for Hand {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.cards.iter().fold("".to_string(), |acc, c| acc + &c.representation.to_string()))
    }
}

impl Eq for Hand {}
impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Hand) -> Option<Ordering> {
        let self_type = self.get_type();
        let other_type = other.get_type();
        if self_type != other_type {
            self_type.partial_cmp(&other_type)
        } else {
            Some(cmp_cards(&self.cards, &other.cards))
        }
    }
}

pub struct Day7 {
    input: String,
}

impl Day7 {
    pub fn new(input: String) -> Day7 {
        Day7 { input }
    }

    fn parse_input(&self, is_part_2: bool) -> Vec<Hand> {
        self.input.lines().map(|line| {
            let parts: Vec<&str> = line.split_whitespace().collect();
            let cards: Vec<Card> = parts[0].chars().map(|c| Card::from_representation(c, is_part_2)).collect();
            let bid: usize = parts[1].parse().unwrap();

            Hand {
                cards,
                bid,
            }
        })
        .collect()
    }
}

impl Solver for Day7 {
    fn part1(&self) -> String {
        let mut input = self.parse_input(false);
        input.sort();
        // println!("{}", input.iter().fold("".to_string(), |acc, h| acc + &h.to_string() + ", "));
        input.iter().enumerate().map(|(i, h)| (i + 1) * h.bid).sum::<usize>().to_string()
    }

    fn part2(&self) -> String {
        let mut input: Vec<Hand> = self.parse_input(true).iter().map(|h| h.apply_jokers()).collect();
        input.sort();
        // println!("{}", input.iter().fold("".to_string(), |acc, h| acc + &h.to_string() + ", "));
        input.iter().enumerate().map(|(i, h)| (i + 1) * h.bid).sum::<usize>().to_string()
    }
}
