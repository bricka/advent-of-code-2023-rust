use std::{fmt::Display, collections::{HashSet, HashMap}, cmp::Reverse};

use num::abs;
use priority_queue::PriorityQueue;

use crate::{solver::Solver, util::slice_to_string};

fn shoelace(points: &[Coord]) -> usize {
    let x_value: i64 = (points.windows(2).map(|window| window[0].x * window[1].y).sum::<usize>() + (points.last().unwrap().x * points[0].y)).try_into().unwrap();
    let y_value: i64 = (points.windows(2).map(|window| window[0].y * window[1].x).sum::<usize>() + (points.last().unwrap().y * points[0].x)).try_into().unwrap();

    abs((x_value - y_value) / 2) as usize
}

#[derive(Hash, PartialEq, Eq, Clone, Debug)]
struct Coord {
    x: usize,
    y: usize,
}

impl Coord {
    fn new(x: usize, y: usize) -> Coord {
        Coord { x, y }
    }

    fn to_south(&self) -> Coord {
        Coord::new(self.x, self.y + 1)
    }

    fn to_north(&self) -> Option<Coord> {
        Some(self.y).filter(|y| *y > 0).map(|y| Coord::new(self.x, y -1))
    }

    fn to_west(&self) -> Option<Coord> {
        Some(self.x).filter(|x| *x > 0).map(|x| Coord::new(x - 1, self.y))
    }

    fn to_east(&self) -> Coord {
        Coord::new(self.x + 1, self.y)
    }
}

impl Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(PartialEq, Clone, Copy)]
enum Pipe {
    NS,
    EW,
    NE,
    NW,
    SW,
    SE,
    Ground,
    Start,
}

impl Pipe {
    fn new(c: char) -> Pipe {
        match c {
            '|' => Pipe::NS,
            '-' => Pipe::EW,
            'L' => Pipe::NE,
            'J' => Pipe::NW,
            '7' => Pipe::SW,
            'F' => Pipe::SE,
            '.' => Pipe::Ground,
            'S' => Pipe::Start,
            _ => panic!("Invalid pipe char: {}", c)
        }
    }

    fn is_ground(&self) -> bool {
        *self == Pipe::Ground
    }

    fn can_connect_north(&self) -> bool {
        match *self {
            Pipe::NS => true,
            Pipe::EW => false,
            Pipe::NE => true,
            Pipe::NW => true,
            Pipe::SW => false,
            Pipe::SE => false,
            Pipe::Ground => false,
            Pipe::Start => true,
        }
    }

    fn can_connect_south(&self) -> bool {
        match *self {
            Pipe::NS => true,
            Pipe::EW => false,
            Pipe::NE => false,
            Pipe::NW => false,
            Pipe::SW => true,
            Pipe::SE => true,
            Pipe::Ground => false,
            Pipe::Start => true,
        }
    }

    fn can_connect_east(&self) -> bool {
        match *self {
            Pipe::NS => false,
            Pipe::EW => true,
            Pipe::NE => true,
            Pipe::NW => false,
            Pipe::SW => false,
            Pipe::SE => true,
            Pipe::Ground => false,
            Pipe::Start => true,
        }
    }

    fn can_connect_west(&self) -> bool {
        match *self {
            Pipe::NS => false,
            Pipe::EW => true,
            Pipe::NE => false,
            Pipe::NW => true,
            Pipe::SW => true,
            Pipe::SE => false,
            Pipe::Ground => false,
            Pipe::Start => true,
        }
    }
}

impl Display for Pipe {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match *self {
            Pipe::NS => '|',
            Pipe::EW => '-',
            Pipe::NE => 'L',
            Pipe::NW => 'J',
            Pipe::SW => '7',
            Pipe::SE => 'F',
            Pipe::Ground => '.',
            Pipe::Start => 'S',
        };

        write!(f, "{}", c)
    }
}

struct Grid {
    grid: Vec<Vec<Pipe>>,
}

impl Grid {
    fn new(g: Vec<Vec<Pipe>>) -> Grid {
        Grid { grid: g }
    }

    fn find_start_coord(&self) -> Coord {
        for (y, row) in self.grid.iter().enumerate() {
            for (x, pipe) in row.iter().enumerate() {
                if *pipe == Pipe::Start {
                    return Coord::new(x, y)
                }
            }
        }

        panic!("No start position found");
    }

    fn get_pipe(&self, c: &Coord) -> Pipe {
        self.grid[c.y][c.x]
    }

    fn get_neighbors(&self, coord: &Coord) -> HashSet<Coord> {
        let pipe = self.get_pipe(coord);
        let neighbors: Vec<Coord> = match pipe {
            Pipe::NS => [
                coord.to_north(),
                Some(coord.to_south()),
            ].iter().flatten().cloned().collect(),
            Pipe::EW => [
                coord.to_west(),
                Some(coord.to_east()),
            ].iter().flatten().cloned().collect(),
            Pipe::NE => [
                coord.to_north(),
                Some(coord.to_east()),
            ].iter().flatten().cloned().collect(),
            Pipe::NW => [
                coord.to_north(),
                coord.to_west(),
            ].iter().flatten().cloned().collect(),
            Pipe::SW => [
                Some(coord.to_south()),
                coord.to_west(),
            ].iter().flatten().cloned().collect(),
            Pipe::SE => [
                coord.to_south(),
                coord.to_east(),
            ].to_vec(),
            Pipe::Ground => vec![],
            Pipe::Start => {
                [
                    coord.to_north(),
                    Some(coord.to_east()),
                    coord.to_west(),
                    Some(coord.to_south()),
                ].iter()
                    .flatten()
                    .filter(|c| self.contains(c))
                    .filter(|c| self.get_neighbors(c).contains(coord))
                    .cloned()
                    .collect()
            }
        };

        HashSet::from_iter(neighbors.iter().filter(|n| self.contains(n)).cloned())
    }

    fn get_clockwise_neighbor(&self, came_from: Option<&Coord>, coord: &Coord) -> Coord {
        let pipe = self.get_pipe(coord);
        let (preferred, non_preferred): (Coord, Coord) = match pipe {
            Pipe::NS => (coord.to_north().unwrap(), coord.to_south()),
            Pipe::EW => (coord.to_east(), coord.to_west().unwrap()),
            Pipe::NE => (coord.to_north().unwrap(), coord.to_east()),
            Pipe::NW => (coord.to_north().unwrap(), coord.to_west().unwrap()),
            Pipe::SW => (coord.to_west().unwrap(), coord.to_south()),
            Pipe::SE => (coord.to_south(), coord.to_east()),
            Pipe::Ground => panic!("Somehow asking about a ground spot"),
            Pipe::Start => {
                return [
                    coord.to_north().filter(|c| self.get_pipe(c).can_connect_south()),
                    Some(coord.to_east()).filter(|c| self.get_pipe(c).can_connect_west()),
                    Some(coord.to_south()).filter(|c| self.get_pipe(c).can_connect_north()),
                    coord.to_west().filter(|c| self.get_pipe(c).can_connect_east()),
                ].iter()
                    .flatten()
                    .filter(|c| self.contains(c))
                    .find(|c| !self.get_pipe(c).is_ground())
                    .unwrap()
                    .clone()
            }
        };

        match came_from {
            None => preferred,
            Some(cf) => if preferred != *cf {
                preferred
            } else {
                non_preferred
            }
        }
    }

    fn contains(&self, c: &Coord) -> bool {
        match self.grid.get(c.y) {
            None => false,
            Some(row) => row.get(c.x).is_some()
        }
    }

    fn to_distance_grid(&self, distances: &HashMap<Coord, usize>) -> String {
        let mut display: String = "".to_string();
        let mut is_first = true;

        for (y, row) in self.grid.iter().enumerate() {
            if !is_first {
                display += "\n";
            }
            is_first = false;

            for (x, _) in row.iter().enumerate() {
                display += &distances.get(&Coord::new(x, y)).map(|d| d.to_string()).unwrap_or(".".to_string());
            }
        }

        display
    }
}

impl Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut display: String = "".to_string();
        let mut is_first = true;

        for y in &self.grid {
            if !is_first {
                display += "\n";
            }
            is_first = false;

            for x in y {
                display += &x.to_string();
            }
        }

        write!(f, "{}", display)
    }
}

pub struct Day10 {
    input: String,
}

impl Day10 {
    pub fn new(input: String) -> Day10 {
        Day10 { input }
    }

    fn parse_input(&self) -> Grid {
        Grid::new(
            self.input.lines().map(|line| {
                line.chars().map(Pipe::new).collect()
            }).collect()
        )
    }
}

impl Solver for Day10 {
    fn part1(&self) -> String {
        let grid = self.parse_input();
        let start_coord = grid.find_start_coord();
        let mut pq: PriorityQueue<Coord, Reverse<usize>> = PriorityQueue::new();
        let mut distances: HashMap<Coord, usize> = HashMap::new();

        grid.get_neighbors(&start_coord).iter().for_each(|n| {
            pq.push(n.clone(), Reverse(0));
        });
        distances.insert(start_coord, 0);

        while let Some((next, distance_so_far)) = pq.pop() {
            let new_distance = distance_so_far.0 + 1;
            let should_visit = match distances.get(&next) {
                None => true,
                Some(&existing_distance) => new_distance < existing_distance,
            };

            if should_visit {
                grid.get_neighbors(&next).iter().for_each(|n| {
                    pq.push(n.clone(), Reverse(new_distance));
                });
                distances.insert(next, new_distance);
            }
        }

        println!("{}", grid.to_distance_grid(&distances));

        distances.values().max().unwrap().to_string()
    }

    fn part2(&self) -> String {
        let grid = self.parse_input();
        let start_coord = grid.find_start_coord();
        let mut current = start_coord.clone();
        let mut path = vec![];

        loop {
            let next = grid.get_clockwise_neighbor(path.last(), &current);
            path.push(current);
            current = next;

            if current == start_coord {
                break
            }
        }

        println!("Path: {}", slice_to_string(&path));

        let area = shoelace(&path);
        let interior_points = area + 1 - (path.len() / 2);

        interior_points.to_string()
    }
}
