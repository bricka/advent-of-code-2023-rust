use std::{ops::Range, collections::HashSet};

pub use crate::solver::Solver;

#[derive(Clone, Debug)]
struct AlmanacRange {
    source_range: Range<usize>,
    dest_range: Range<usize>,
}

impl AlmanacRange {
    fn new(source_range_start: usize, dest_range_start: usize, range_length: usize) -> AlmanacRange {
        AlmanacRange {
            source_range: Range {
                start: source_range_start,
                end: source_range_start + range_length,
            },
            dest_range: Range {
                start: dest_range_start,
                end: dest_range_start + range_length,
            },
        }
    }
    fn convert(&self, n: usize) -> Option<usize> {
        if !self.source_range.contains(&n) {
            None
        } else {
            Some(self.dest_range.start + n - self.source_range.start)
        }
    }
}

#[derive(Debug)]
struct RangeMap {
    ranges: Vec<AlmanacRange>,
}

impl RangeMap {
    fn new(ranges: &[AlmanacRange]) -> RangeMap {
        let mut sorted_ranges = ranges.to_vec();
        sorted_ranges.sort_by_key(|r| r.source_range.start);

        RangeMap {
            ranges: sorted_ranges.to_vec(),
        }
    }

    fn convert(&self, n: usize) -> usize {
        self.ranges.iter().find_map(|r| r.convert(n)).unwrap_or(n)
    }

    fn convert_all(&self, input: &[Range<usize>]) -> Vec<Range<usize>> {
        let mut out = vec![];

        for range in input {
            println!("Processing range {:?}", range);
            let mut current_number = range.start;
            let relevant_ranges = self.ranges.iter().filter(|ar| ar.source_range.end > current_number).collect::<Vec<&AlmanacRange>>();
            if relevant_ranges.is_empty() {
                out.push(range.clone());
                continue;
            }

            let (mut current_range, mut remaining_ranges) = if relevant_ranges[0].source_range.contains(&current_number) {
                let (first, rest) = relevant_ranges.split_first().unwrap();
                (Some(*first), rest)
            } else {
                (None, relevant_ranges.as_slice())
            };

            println!("Starting current number = {:?}, current range = {:?}, remaining = {:?}", current_number, current_range, remaining_ranges);

            while range.contains(&current_number) {
                match current_range {
                    None => {
                        // We are between ranges of the RangeMap
                        if let Some((next_range, new_remaining_ranges)) = remaining_ranges.split_first() {
                            println!("Next range = {:?}", next_range);
                            let next_cn = next_range.source_range.start;
                            let new_range = Range { start: current_number, end: next_cn };
                            println!("Adding range (remaining) {:?}", new_range);
                            out.push(new_range);
                            current_number = next_cn;
                            current_range = Some(next_range);
                            remaining_ranges = new_remaining_ranges;
                        } else {
                            let new_range = Range { start: current_number, end: range.end };
                            println!("Adding range (no more) {:?}", new_range);
                            out.push(new_range);
                            break;
                        }
                    },
                    Some(cr) => {
                        // We are currently in a range of the RangeMap
                        if range.end >= cr.source_range.end {
                            // This range in the RangeMap does not fully cover the input range
                            let next_cn = cr.source_range.end;
                            out.push(Range {
                                start: cr.convert(current_number).unwrap(),
                                end: cr.convert(cr.source_range.end - 1).unwrap() + 1,
                            });
                            current_number = next_cn;
                            match remaining_ranges.split_first() {
                                None => {
                                    // No remaining ranges left
                                    current_range = None;
                                },
                                Some((rr_first, rr_rest)) => {
                                    if rr_first.source_range.contains(&current_number) {
                                        current_range = Some(rr_first);
                                        remaining_ranges = rr_rest;
                                    } else {
                                        current_range = None;
                                    }
                                }
                            }
                        } else {
                            // This range in the RangeMap fully covers this input range
                            out.push(Range {
                                start: cr.convert(current_number).unwrap(),
                                end: cr.convert(range.end).unwrap(),
                            });
                            break;
                        }
                    }
                }
            }
        }

        out.sort_by_key(|r| r.start);

        out
    }
}

#[derive(Debug)]
struct Part1PuzzleInput {
    seeds: HashSet<usize>,
    seed_to_soil: RangeMap,
    soil_to_fertilizer: RangeMap,
    fertilizer_to_water: RangeMap,
    water_to_light: RangeMap,
    light_to_temperature: RangeMap,
    temperature_to_humidity: RangeMap,
    humidity_to_location: RangeMap,
}

#[derive(Debug)]
struct Part2PuzzleInput {
    seed_ranges: Vec<Range<usize>>,
    seed_to_soil: RangeMap,
    soil_to_fertilizer: RangeMap,
    fertilizer_to_water: RangeMap,
    water_to_light: RangeMap,
    light_to_temperature: RangeMap,
    temperature_to_humidity: RangeMap,
    humidity_to_location: RangeMap,
}

fn is_seeds_line(line: &str) -> bool {
    line.starts_with("seeds: ")
}

fn part_1_parse_seeds_line(line: &str) -> HashSet<usize> {
    let nums: Vec<usize> = line.split(": ")
        .collect::<Vec<&str>>()
        .get(1)
        .unwrap()
        .split_whitespace()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();

    let mut set = HashSet::new();
    for num in nums {
        set.insert(num);
    }
    return set;
}

fn part_2_parse_seeds_line(line: &str) -> Vec<Range<usize>> {
    let nums: Vec<usize> = line.split(": ")
        .collect::<Vec<&str>>()
        .get(1)
        .unwrap()
        .split_whitespace()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();

    nums.as_slice().chunks(2).map(|chunk| Range {
        start: chunk[0],
        end: chunk[0] + chunk[1],
    }).collect()
}

fn is_map_line(line: &str) -> bool {
    line.ends_with(" map:")
}

fn parse_map_name(line: &str) -> String {
    line.split_whitespace().collect::<Vec<&str>>().first().unwrap().to_string()
}

fn is_range_line(line: &str) -> bool {
    let parts: Vec<&str> = line.split_whitespace().collect();
    if parts.len() != 3 {
        false
    } else {
        parts.iter().all(|p| p.parse::<usize>().is_ok())
    }
}

fn parse_range_line(line: &str) -> AlmanacRange {
    let parts: Vec<usize> = line.split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect();
    AlmanacRange::new(*parts.get(1).unwrap(), *parts.first().unwrap(), *parts.get(2).unwrap())
}

pub struct Day5 {
    input: String,
}

impl Day5 {
    pub fn new(input: String) -> Day5 {
        Day5 {
            input,
        }
    }

    fn part_1_parse_input(&self) -> Part1PuzzleInput {
        let mut seeds: Option<HashSet<usize>> = None;

        let mut current_map_name: Option<String> = None;
        let mut current_ranges: Vec<AlmanacRange> = vec![];
        let mut seed_to_soil: Option<RangeMap> = None;
        let mut soil_to_fertilizer: Option<RangeMap> = None;
        let mut fertilizer_to_water: Option<RangeMap> = None;
        let mut water_to_light: Option<RangeMap> = None;
        let mut light_to_temperature: Option<RangeMap> = None;
        let mut temperature_to_humidity: Option<RangeMap> = None;
        let mut humidity_to_location: Option<RangeMap> = None;

        self.input.lines().for_each(|line| {
            if is_seeds_line(line) {
                seeds = Some(part_1_parse_seeds_line(line));
            } else if is_map_line(line) {
                current_map_name = Some(parse_map_name(line));
            } else if is_range_line(line) {
                current_ranges.push(parse_range_line(line));
            } else if line.is_empty() {
                if current_map_name.is_some() {
                    let map = RangeMap::new(&current_ranges);
                    let actual_map_name = current_map_name.as_ref().unwrap().as_str();
                    match actual_map_name {
                        "seed-to-soil" => seed_to_soil = Some(map),
                        "soil-to-fertilizer" => soil_to_fertilizer = Some(map),
                        "fertilizer-to-water" => fertilizer_to_water = Some(map),
                        "water-to-light" => water_to_light = Some(map),
                        "light-to-temperature" => light_to_temperature = Some(map),
                        "temperature-to-humidity" => temperature_to_humidity = Some(map),
                        "humidity-to-location" => humidity_to_location = Some(map),
                        _ => panic!("Unknown map: {}", actual_map_name),
                    }

                    current_map_name = None;
                    current_ranges = vec![];
                } else {
                    eprintln!("Empty line, but no map!");
                }
            } else {
                panic!("Unknown line: {}", line)
            }
        });

        let map = RangeMap::new(&current_ranges);
        let actual_map_name = current_map_name.as_ref().unwrap().as_str();
        match actual_map_name {
            "seed-to-soil" => seed_to_soil = Some(map),
            "soil-to-fertilizer" => soil_to_fertilizer = Some(map),
            "fertilizer-to-water" => fertilizer_to_water = Some(map),
            "water-to-light" => water_to_light = Some(map),
            "light-to-temperature" => light_to_temperature = Some(map),
            "temperature-to-humidity" => temperature_to_humidity = Some(map),
            "humidity-to-location" => humidity_to_location = Some(map),
            _ => panic!("Unknown map: {}", actual_map_name),
        }

        Part1PuzzleInput {
            seeds: seeds.unwrap(),
            seed_to_soil: seed_to_soil.unwrap(),
            soil_to_fertilizer: soil_to_fertilizer.unwrap(),
            fertilizer_to_water: fertilizer_to_water.unwrap(),
            water_to_light: water_to_light.unwrap(),
            light_to_temperature: light_to_temperature.unwrap(),
            temperature_to_humidity: temperature_to_humidity.unwrap(),
            humidity_to_location: humidity_to_location.unwrap(),
        }
    }

    fn part_2_parse_input(&self) -> Part2PuzzleInput {
        let mut seeds: Option<Vec<Range<usize>>> = None;

        let mut current_map_name: Option<String> = None;
        let mut current_ranges: Vec<AlmanacRange> = vec![];
        let mut seed_to_soil: Option<RangeMap> = None;
        let mut soil_to_fertilizer: Option<RangeMap> = None;
        let mut fertilizer_to_water: Option<RangeMap> = None;
        let mut water_to_light: Option<RangeMap> = None;
        let mut light_to_temperature: Option<RangeMap> = None;
        let mut temperature_to_humidity: Option<RangeMap> = None;
        let mut humidity_to_location: Option<RangeMap> = None;

        self.input.lines().for_each(|line| {
            if is_seeds_line(line) {
                seeds = Some(part_2_parse_seeds_line(line));
            } else if is_map_line(line) {
                current_map_name = Some(parse_map_name(line));
            } else if is_range_line(line) {
                current_ranges.push(parse_range_line(line));
            } else if line.is_empty() {
                if current_map_name.is_some() {
                    let map = RangeMap::new(&current_ranges);
                    let actual_map_name = current_map_name.as_ref().unwrap().as_str();
                    match actual_map_name {
                        "seed-to-soil" => seed_to_soil = Some(map),
                        "soil-to-fertilizer" => soil_to_fertilizer = Some(map),
                        "fertilizer-to-water" => fertilizer_to_water = Some(map),
                        "water-to-light" => water_to_light = Some(map),
                        "light-to-temperature" => light_to_temperature = Some(map),
                        "temperature-to-humidity" => temperature_to_humidity = Some(map),
                        "humidity-to-location" => humidity_to_location = Some(map),
                        _ => panic!("Unknown map: {}", actual_map_name),
                    }

                    current_map_name = None;
                    current_ranges = vec![];
                } else {
                    eprintln!("Empty line, but no map!");
                }
            } else {
                panic!("Unknown line: {}", line)
            }
        });

        let map = RangeMap::new(&current_ranges);
        let actual_map_name = current_map_name.as_ref().unwrap().as_str();
        match actual_map_name {
            "seed-to-soil" => seed_to_soil = Some(map),
            "soil-to-fertilizer" => soil_to_fertilizer = Some(map),
            "fertilizer-to-water" => fertilizer_to_water = Some(map),
            "water-to-light" => water_to_light = Some(map),
            "light-to-temperature" => light_to_temperature = Some(map),
            "temperature-to-humidity" => temperature_to_humidity = Some(map),
            "humidity-to-location" => humidity_to_location = Some(map),
            _ => panic!("Unknown map: {}", actual_map_name),
        }

        Part2PuzzleInput {
            seed_ranges: seeds.unwrap(),
            seed_to_soil: seed_to_soil.unwrap(),
            soil_to_fertilizer: soil_to_fertilizer.unwrap(),
            fertilizer_to_water: fertilizer_to_water.unwrap(),
            water_to_light: water_to_light.unwrap(),
            light_to_temperature: light_to_temperature.unwrap(),
            temperature_to_humidity: temperature_to_humidity.unwrap(),
            humidity_to_location: humidity_to_location.unwrap(),
        }
    }
}

impl Solver for Day5 {
    fn part1(&self) -> String {
        let input = self.part_1_parse_input();

        input.seeds.iter().map(|seed| {
            let soil = input.seed_to_soil.convert(*seed);
            let fertilizer = input.soil_to_fertilizer.convert(soil);
            let water = input.fertilizer_to_water.convert(fertilizer);
            let light = input.water_to_light.convert(water);
            let temperature = input.light_to_temperature.convert(light);
            let humidity = input.temperature_to_humidity.convert(temperature);

            input.humidity_to_location.convert(humidity)
        }).min().unwrap().to_string()
    }

    fn part2(&self) -> String {
        let input = self.part_2_parse_input();
        let mut seed_ranges = input.seed_ranges;
        seed_ranges.sort_by_key(|r| r.start);

        let soils = input.seed_to_soil.convert_all(&seed_ranges);
        let fertilizers = input.soil_to_fertilizer.convert_all(&soils);
        let waters = input.fertilizer_to_water.convert_all(&fertilizers);
        let lights = input.water_to_light.convert_all(&waters);
        let temperatures = input.light_to_temperature.convert_all(&lights);
        let humidities = input.temperature_to_humidity.convert_all(&temperatures);
        let locations = input.humidity_to_location.convert_all(&humidities);

        locations.iter().map(|r| r.start).min().unwrap().to_string()
    }
}
