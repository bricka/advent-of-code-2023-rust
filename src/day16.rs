use std::{fmt::Display, fmt::Debug, collections::HashSet};

use array2d::Array2D;

use crate::{solver::Solver, point::Point};

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    fn next(&self, point: &Point) -> Option<Point> {
        match self {
            Direction::Left => point.left(),
            Direction::Right => Some(Point { x: point.x + 1, y: point.y }),
            Direction::Up => point.up(),
            Direction::Down => Some(Point { x: point.x, y: point.y + 1 }),
        }
    }
}

impl Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Left => write!(f, "Left"),
            Self::Right => write!(f, "Right"),
            Self::Up => write!(f, "Up"),
            Self::Down => write!(f, "Down"),
        }
    }
}

impl Debug for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

#[derive(Clone, Copy)]
enum Tile {
    EmptySpace,
    MirrorLeftUp,
    MirrorRightUp,
    SplitterVert,
    SplitterHoriz,
}

impl Tile {
    fn when_beam_hits(&self, loc: Point, dir: Direction) -> Vec<(Point, Direction)> {
        match (self, dir) {
            (Tile::EmptySpace, _) => dir.next(&loc).map(|p| vec![(p, dir)]).unwrap_or(vec![]),
            (Tile::MirrorLeftUp, Direction::Left) => vec![(Point { x: loc.x, y: loc.y + 1 }, Direction::Down)],
            (Tile::MirrorLeftUp, Direction::Right) => loc.up().map(|p| vec![(p, Direction::Up)]).unwrap_or(vec![]),
            (Tile::MirrorLeftUp, Direction::Up) => vec![(Point { x: loc.x + 1, y: loc.y }, Direction::Right)],
            (Tile::MirrorLeftUp, Direction::Down) => loc.left().map(|p| vec![(p, Direction::Left)]).unwrap_or(vec![]),
            (Tile::MirrorRightUp, Direction::Left) => loc.up().map(|p| vec![(p, Direction::Up)]).unwrap_or(vec![]),
            (Tile::MirrorRightUp, Direction::Right) => vec![(Point { x: loc.x, y: loc.y + 1 }, Direction::Down)],
            (Tile::MirrorRightUp, Direction::Up) => loc.left().map(|p| vec![(p, Direction::Left)]).unwrap_or(vec![]),
            (Tile::MirrorRightUp, Direction::Down) => vec![(Point { x: loc.x + 1, y: loc.y }, Direction::Right)],
            (Tile::SplitterVert, Direction::Up | Direction::Down) => dir.next(&loc).map(|p| vec![(p, dir)]).unwrap_or(vec![]),
            (Tile::SplitterVert, Direction::Left | Direction::Right) => [
                loc.up().map(|p| (p, Direction::Up)),
                Some((Point { x: loc.x, y: loc.y + 1 }, Direction::Down)),
            ].iter().flatten().cloned().collect(),
            (Tile::SplitterHoriz, Direction::Left | Direction::Right) => dir.next(&loc).map(|p| vec![(p, dir)]).unwrap_or(vec![]),
            (Tile::SplitterHoriz, Direction::Up | Direction::Down) => [
                loc.left().map(|p| (p, Direction::Left)),
                Some((Point { x: loc.x + 1, y: loc.y }, Direction::Right)),
            ].iter().flatten().cloned().collect(),
        }
    }
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            '.' => Tile::EmptySpace,
            '/' => Tile::MirrorLeftUp,
            '\\' => Tile::MirrorRightUp,
            '|' => Tile::SplitterVert,
            '-' => Tile::SplitterHoriz,
            _ => panic!("Unknown tile: {}", value),
        }
    }
}

struct Grid {
    tiles: Array2D<Tile>,
}

impl Grid {
    fn energize(&self, start_point: &Point, start_direction: Direction) -> (Array2D<bool>, HashSet<(Point, Direction)>) {
        let mut seen_states: HashSet<(Point, Direction)> = HashSet::new();
        let mut energized_grid = Array2D::filled_with(false, self.tiles.num_rows(), self.tiles.num_columns());
        let mut beams = vec![(start_point.clone(), start_direction)];

        while let Some((loc, dir)) = beams.pop() {
            if seen_states.contains(&(loc.clone(), dir)) {
                continue
            }
            seen_states.insert((loc.clone(), dir));
            // println!("Loc {}, dir {}", loc, dir);
            if energized_grid.get(loc.y, loc.x).is_some() {
                energized_grid[(loc.y, loc.x)] = true;
                beams.append(&mut self.tiles[(loc.y, loc.x)].when_beam_hits(loc, dir));
            }
        }

        (energized_grid, seen_states)
    }
}

pub struct Day16 {
    input: String,
}

impl Day16 {
    pub fn new(input: String) -> Day16 {
        Day16 { input }
    }

    fn parse_input(&self) -> Grid {
        let rows: Vec<Vec<Tile>> = self.input.lines()
            .map(|line| line.chars().map(Tile::from).collect::<Vec<Tile>>())
            .collect();

        Grid {
            tiles: Array2D::from_rows(&rows).unwrap(),
        }
    }
}

impl Solver for Day16 {
    fn part1(&self) -> String {
        self.parse_input()
            .energize(&Point { x: 0, y: 0 }, Direction::Right)
            .0
            .rows_iter()
            .fold(0, |acc, row| acc + row.filter(|&&energized| energized).count())
            .to_string()
    }

    fn part2(&self) -> String {
        let grid = self.parse_input();
        let max_x = grid.tiles.num_columns() - 1;
        let max_y = grid.tiles.num_rows() - 1;

        let mut seen_states: HashSet<(Point, Direction)> = HashSet::new();
        let mut max_so_far = 0;

        let starting_beams: Vec<(Point, Direction)> = (0..grid.tiles.num_columns())
            .flat_map(|y| vec![
                (Point { x: 0, y }, Direction::Right),
                (Point { x: max_x, y }, Direction::Left),
            ])
            .chain(
                (0..grid.tiles.num_rows())
                    .flat_map(|x| vec![
                        (Point { x, y: 0}, Direction::Down),
                        (Point { x, y: max_y }, Direction::Up),
                    ])
            )
            .collect();

        for beam in &starting_beams {
            if seen_states.contains(beam) {
                continue
            }

            let (energized, seen) = grid.energize(&beam.0, beam.1);
            seen_states.extend(seen);
            let new_count = energized.rows_iter().fold(0, |acc, row| acc + row.filter(|&&e| e).count());

            if new_count > max_so_far {
                max_so_far = new_count;
            }
        }

        max_so_far.to_string()
    }
}
