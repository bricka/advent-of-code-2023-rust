use num::BigUint;

use crate::{solver::Solver, direction::Direction, point::Point};

fn shoelace(points: &[Point]) -> BigUint {
    let x_value: BigUint = points.windows(2)
        .map(|window| BigUint::from(window[0].x) * BigUint::from(window[1].y))
        .sum::<BigUint>()
        +
        (BigUint::from(points.last().unwrap().x) * BigUint::from(points[0].y));

    let y_value: BigUint = points.windows(2)
        .map(|window| BigUint::from(window[0].y) * BigUint::from(window[1].x))
        .sum::<BigUint>()
        +
        points.last().unwrap().y * points[0].x;

    if x_value > y_value {
        (x_value - y_value) / 2_usize
    } else {
        (y_value - x_value) / 2_usize
    }
}

pub struct Day18 {
    input: String,
}

impl Day18 {
    pub fn new(input: String) -> Day18 {
        Day18 { input }
    }

    fn parse_input_part_1(&self) -> Vec<(Direction, usize)> {
        self.input.lines()
            .map(|line| {
                let parts: Vec<&str> = line.split_whitespace().collect();
                (Direction::from(parts[0].chars().next().unwrap()), parts[1].parse().unwrap())
            })
            .collect()
    }

    fn parse_input_part_2(&self) -> Vec<(Direction, usize)> {
        self.input.lines()
            .map(|line| {
                let wrapped_hex = line.split_whitespace().last().unwrap();
                let (len_part, dir_part) = wrapped_hex[2..wrapped_hex.len()-1].split_at(5);
                let dir = match dir_part.chars().next().unwrap() {
                    '0' => Direction::Right,
                    '1' => Direction::Down,
                    '2' => Direction::Left,
                    '3' => Direction::Up,
                    _ => panic!("Invalid direction number"),
                };
                (dir, usize::from_str_radix(len_part, 16).unwrap())
            })
            .collect()
    }

    fn solve_with_input(instructions: &[(Direction, usize)]) -> BigUint {
        let mut current = Point::new(100_000_000, 100_000_000);
        let mut points = vec![];

        for &(dir, len) in instructions {
            for _ in 0..len {
                current = current.in_direction(dir).unwrap();
                points.push(current.clone());
            }
        }

        let area = shoelace(&points);
        let interior_points = area + 1_usize - (points.len() / 2);

        interior_points + points.len()
    }
}

impl Solver for Day18 {
    fn part1(&self) -> String {
        Day18::solve_with_input(&self.parse_input_part_1()).to_string()
    }

    fn part2(&self) -> String {
        Day18::solve_with_input(&self.parse_input_part_2()).to_string()
    }
}
