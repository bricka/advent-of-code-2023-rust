use std::collections::HashMap;
use regex::Regex;

use crate::solver::Solver;
use num::integer::lcm;

lazy_static! {
    static ref LINE_REGEX: Regex = Regex::new(r"^(\w+) = \((\w+), (\w+)\)$").expect("Regex should be valid");
}

fn lcm_n(nums: &[usize]) -> usize {
    nums.iter().fold(1, |acc, n| lcm(acc, *n))
}

#[derive(Hash, PartialEq, Eq, Clone)]
struct Location {
    name: String,
}

impl Location {
    fn new(name: &str) -> Location {
        Location { name: name.to_string() }
    }

    fn is_start_field(&self) -> bool {
        self.name.ends_with('A')
    }

    fn is_end_field(&self) -> bool {
        self.name.ends_with('Z')
    }
}

enum Instruction {
    Left,
    Right,
}

impl From<char> for Instruction {
    fn from(value: char) -> Self {
        match value {
            'L' => Instruction::Left,
            'R' => Instruction::Right,
            _ => panic!("Invalid instruction: {}", value),
        }
    }
}

#[derive(Clone)]
struct Instructions {
    instruction_prompt: Vec<char>,
    next_index: usize,
}

impl Instructions {
    fn new(prompt: &str) -> Instructions {
        Instructions { instruction_prompt: prompt.chars().collect(), next_index: 0 }
    }
}

impl Iterator for Instructions {
    type Item = Instruction;

    fn next(&mut self) -> Option<Self::Item> {
        let next_val = self.instruction_prompt[self.next_index];
        self.next_index = if self.next_index == self.instruction_prompt.len() - 1 {
            0
        } else {
            self.next_index + 1
        };

        Some(Instruction::from(next_val))
    }
}

struct NextNode {
    left: Location,
    right: Location,
}

impl NextNode {
    fn new(left: &str, right: &str) -> NextNode {
        NextNode {
            left: Location::new(left),
            right: Location::new(right),
        }
    }

    fn get_next(&self, i: Instruction) -> &Location {
        match i {
            Instruction::Left => &self.left,
            Instruction::Right => &self.right,
        }
    }
}

struct PuzzleInput {
    instructions: Instructions,
    next_nodes: HashMap<Location, NextNode>,
}

pub struct Day8 {
    input: String,
}

impl Day8 {
    pub fn new(input: String) -> Day8 {
        Day8 { input }
    }

    fn parse_input(&self) -> PuzzleInput {
        let lines: Vec<&str> = self.input
            .lines()
            .collect();

        let instructions = Instructions::new(lines[0]);

        let next_nodes = lines.iter().skip(2).map(|line| {
            let captures = LINE_REGEX.captures(line).unwrap();
            let (_, [source, left, right]) = captures.extract();
            (Location::new(source), NextNode::new(left, right))
        })
        .collect();

        PuzzleInput {
            instructions,
            next_nodes,
        }
    }
}

impl Solver for Day8 {
    fn part1(&self) -> String {
        let mut input = self.parse_input();
        let mut step_count = 0;
        let mut current = Location::new("AAA");

        while current != Location::new("ZZZ") {
            let i = input.instructions.next().unwrap();
            current = input.next_nodes[&current].get_next(i).clone();
            step_count += 1;
        }

        step_count.to_string()
    }

    fn part2(&self) -> String {
        let input = self.parse_input();

        let starts: Vec<&Location> = input.next_nodes.keys().filter(|k| k.is_start_field()).collect();
        let distances: Vec<usize> = starts.iter()
            .map(|start| {
                let mut instructions = input.instructions.clone();
                let mut current = *start;
                let mut step_count = 0_usize;
                while !current.is_end_field() {
                    let i = instructions.next().unwrap();
                    current = input.next_nodes[current].get_next(i);
                    step_count += 1;
                }

                step_count
            })
            .collect();
        lcm_n(&distances).to_string()
    }
}
