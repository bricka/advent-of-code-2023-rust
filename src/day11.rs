use std::{collections::HashSet, fmt::Display};

use itertools::Itertools;
use num::abs;

use crate::solver::Solver;

#[derive(PartialEq, Eq)]
struct Point {
    x: i64,
    y: i64,
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }
}

fn manhattan_distance(p1: &Point, p2: &Point) -> i64 {
    abs(p1.x - p2.x) + abs(p1.y - p2.y)
}

struct SparseGrid {
    points: Vec<Point>,
}

impl SparseGrid {
    fn new(points: Vec<Point>) -> SparseGrid {
        SparseGrid { points }
    }

    fn expand(&mut self, add_to_expanded_columns: i64) {
        let max_x = self.points.iter().map(|p| p.x).max().unwrap();
        let max_y = self.points.iter().map(|p| p.y).max().unwrap();

        let empty_cols: HashSet<i64> = (0..=max_x).filter(|&x| self.points_in_col(x).is_empty()).collect();
        let empty_rows: HashSet<i64> = (0..=max_y).filter(|&y| self.points_in_row(y).is_empty()).collect();

        for p in self.points.iter_mut() {
            let num_empty_cols_before_me: i64 = empty_cols.iter().filter(|&&x| x < p.x).count().try_into().unwrap();
            let num_empty_rows_before_me: i64 = empty_rows.iter().filter(|&&y| y < p.y).count().try_into().unwrap();

            p.x += num_empty_cols_before_me * add_to_expanded_columns;
            p.y += num_empty_rows_before_me * add_to_expanded_columns;
        }
    }

    fn points_in_col(&self, col: i64) -> Vec<&Point> {
        self.points.iter().filter(|p| p.x == col).collect()
    }

    fn points_in_row(&self, row: i64) -> Vec<&Point> {
        self.points.iter().filter(|p| p.y == row).collect()
    }

    fn is_present(&self, x: i64, y: i64) -> bool {
        self.points.contains(&Point { x, y })
    }

    fn all_combinations(&self) -> Vec<(&Point, &Point)> {
        self.points.iter().tuple_combinations().collect()
    }
}

impl Display for SparseGrid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let max_x = self.points.iter().map(|p| p.x).max().unwrap();
        let max_y = self.points.iter().map(|p| p.y).max().unwrap();

        let display: String = (0..=max_y).map(|y| {
            (0..=max_x).map(|x| match self.is_present(x, y) {
                false => '.',
                true => '#',
            }).collect::<String>()
        })
            .collect::<Vec<String>>()
            .join("\n");
        write!(f, "{}", display)
    }
}

pub struct Day11 {
    input: String,
}

impl Day11 {
    pub fn new(input: String) -> Day11 {
        Day11 { input }
    }

    fn parse_input(&self) -> SparseGrid {
        SparseGrid::new(
            self.input.lines()
                .enumerate()
                .flat_map(|(y, line)| {
                    line.chars()
                        .enumerate()
                        .filter_map(move |(x, c)| match c {
                            '#' => Some(Point::new(
                                x.try_into().unwrap(),
                                y.try_into().unwrap())
                            ),
                            _ => None,
                        })
                })
                .collect()
        )
    }
}

impl Solver for Day11 {
    fn part1(&self) -> String {
        let mut input = self.parse_input();
        // println!("Raw:\n{}", input);

        input.expand(1);
        // println!("Expanded:\n{}", input);

        // println!(
        //     "{{{}}}",
        //     input.all_combinations().iter()
        //         .map(|(p1, p2)| format!("({}, {})", p1, p2))
        //         .collect::<Vec<String>>()
        //         .join(", ")
        // );

        input.all_combinations().iter().map(|(p1, p2)| manhattan_distance(p1, p2)).sum::<i64>().to_string()
    }

    fn part2(&self) -> String {
        let mut input = self.parse_input();
        // println!("Raw:\n{}", input);
        input.expand(999999);
        // println!("Expanded:\n{}", input);
        input.all_combinations().iter().map(|(p1, p2)| manhattan_distance(p1, p2)).sum::<i64>().to_string()
    }
}
