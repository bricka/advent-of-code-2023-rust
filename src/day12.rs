use std::{fmt::Display, collections::HashMap};

use crate::{solver::Solver, util::slice_to_string};

fn parse_spring_conditions_part(part: &str) -> Vec<SpringCondition> {
    part.chars().map(SpringCondition::from).collect()
}

fn parse_groups_part(part: &str) -> Vec<usize> {
    part.split(',').map(|s| s.parse().unwrap()).collect()
}

#[derive(Clone, Copy)]
enum SpringCondition {
    Operational,
    Damaged,
    Unknown,
}

impl From<char> for SpringCondition {
    fn from(value: char) -> Self {
        match value {
            '.' => SpringCondition::Operational,
            '#' => SpringCondition::Damaged,
            '?' => SpringCondition::Unknown,
            _ => panic!("Unknown condition value: {}", value)
        }
    }
}

impl Display for SpringCondition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            SpringCondition::Operational => '.',
            SpringCondition::Damaged => '#',
            SpringCondition::Unknown => '?',
        })
    }
}

struct ConditionRecord {
    spring_conditions: Vec<SpringCondition>,
    broken_groups: Vec<usize>,
}

impl ConditionRecord {
    fn from_parts(spring_conditions_part: &str, groups_part: &str) -> ConditionRecord {
        ConditionRecord {
            spring_conditions: parse_spring_conditions_part(spring_conditions_part),
            broken_groups: parse_groups_part(groups_part),
        }
    }

    fn count_ways_to_fill(&self) -> usize {
        count_ways_to_fill_with_cache(
            &mut HashMap::new(),
            &self.spring_conditions,
            None,
            &self.broken_groups
        )
    }

    fn extend_by(&mut self, n: usize) {
        let mut i = 1;

        let broken_groups_to_extend = &self.broken_groups.clone();
        let spring_conditions_to_extend = &self.spring_conditions.clone();

        while i < n {
            self.broken_groups.extend(broken_groups_to_extend);
            self.spring_conditions.push(SpringCondition::Unknown);
            self.spring_conditions.extend(spring_conditions_to_extend);
            i += 1;
        }
    }
}

/// Calculate all of the ways to fill a particular set of springs with a particular set of groups.
///
/// The reason for the map key type of `(usize, Option<usize>, usize)` is to get a type that doesn't require us to:
/// 1. Use a reference in the key, because that requires a lot of borrow-wrangling
/// 2. Need to collect the references into vectors, which causes a lot of memory allocation
///
/// Because a single cache is used for each `CacheResult`, we know that a given length and a given number of remaining groups is unique.
fn count_ways_to_fill_with_cache(cache: &mut HashMap<(usize, Option<usize>, usize), usize>, spring_conditions: &[SpringCondition], would_need_available_to_fill: Option<usize>, remaining_groups: &[usize]) -> usize {
    let cache_key = (spring_conditions.len(), would_need_available_to_fill, remaining_groups.len());

    let count = match spring_conditions.split_first() {
        None => match (would_need_available_to_fill, remaining_groups.len()) {
            (None, 0) => 1,
            (Some(x), 1) if x == remaining_groups[0] => 1,
            _ => 0,
        }
        Some((first_spring, rest_springs)) => if remaining_groups.is_empty() && would_need_available_to_fill.is_some() {
            0
        } else {
            match cache.get(&cache_key) {
                Some(count) => *count,
                None => match (first_spring, would_need_available_to_fill) {
                    (SpringCondition::Operational, None) => count_ways_to_fill_with_cache(cache, rest_springs, would_need_available_to_fill, remaining_groups),
                    (SpringCondition::Operational, Some(x)) if x == remaining_groups[0] => count_ways_to_fill_with_cache(cache, rest_springs, None, &remaining_groups[1..]),
                    (SpringCondition::Operational, Some(_)) => 0,

                    (SpringCondition::Damaged, x) => count_ways_to_fill_with_cache(cache, rest_springs, x.map(|n| n + 1).or(Some(1)), remaining_groups),

                    (SpringCondition::Unknown, None) =>
                    // if we treat as Operational:
                        count_ways_to_fill_with_cache(cache, rest_springs, would_need_available_to_fill, remaining_groups) +
                    // if we treat as Damaged:
                        count_ways_to_fill_with_cache(cache, rest_springs, Some(1), remaining_groups),
                    (SpringCondition::Unknown, Some(x)) if x == remaining_groups[0] =>
                    // if we treat as Operational:
                        count_ways_to_fill_with_cache(cache, rest_springs, None, &remaining_groups[1..]) +
                    // if we treat as Damaged:
                        count_ways_to_fill_with_cache(cache, rest_springs, Some(x + 1), remaining_groups),
                    (SpringCondition::Unknown, Some(x)) =>
                    // if we treat as Operational, 0
                    // if we treat as Damaged:
                        count_ways_to_fill_with_cache(cache, rest_springs, Some(x + 1), remaining_groups),
                }
            }
        }
    };

    cache.insert(cache_key, count);
    count
}

impl Display for ConditionRecord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{ {} {} }}",
               self.spring_conditions.iter().fold("".to_string(), |acc, c| acc + &c.to_string()),
               slice_to_string(&self.broken_groups),
        )
    }
}

pub struct Day12 {
    input: String,
}

impl Day12 {
    pub fn new(input: String) -> Day12 {
        Day12 { input }
    }

    fn parse_input(&self) -> Vec<ConditionRecord> {
        self.input.lines()
            .map(|line| line.split_whitespace().collect::<Vec<&str>>())
            .map(|parts| ConditionRecord::from_parts(parts[0], parts[1]))
            .collect()
    }
}

impl Solver for Day12 {
    fn part1(&self) -> String {
        let input = self.parse_input();
        // println!("{}", slice_to_string(&input));
        input.iter()
            .map(|cr| cr.count_ways_to_fill())
            .sum::<usize>()
            .to_string()
    }

    fn part2(&self) -> String {
        let mut input = self.parse_input();
        input.iter_mut().for_each(|cr| cr.extend_by(5));

        println!("{}", slice_to_string(&input));
        input.iter()
            .map(|cr| cr.count_ways_to_fill())
            .sum::<usize>()
            .to_string()
    }
}
