use std::collections::{HashMap, VecDeque};

use num::integer::lcm;

use crate::{solver::Solver, util::slice_to_string};

#[derive(Clone, Copy, PartialEq, Eq)]
enum Pulse {
    Low,
    High,
}

trait Module {
    fn send(&mut self, from: Option<String>, pulse: &Pulse) -> Vec<(String, Pulse)>;
}

struct BroadcasterModule {
    destinations: Vec<String>,
}

impl BroadcasterModule {
    fn new(destinations: &[&str]) -> BroadcasterModule {
        BroadcasterModule {
            destinations: destinations.iter().map(|s| s.to_string()).collect(),
        }
    }
}

impl Module for BroadcasterModule {
    fn send(&mut self, _from: Option<String>, pulse: &Pulse) -> Vec<(String, Pulse)> {
        self.destinations.iter()
            .map(|d| (d.clone(), *pulse))
            .collect()
    }
}

struct FlipFlopModule {
    is_on: bool,
    destinations: Vec<String>,
}

impl FlipFlopModule {
    fn new(destinations: &[&str]) -> FlipFlopModule {
        FlipFlopModule {
            is_on: false,
            destinations: destinations.iter().map(|s| s.to_string()).collect(),
        }
    }
}

impl Module for FlipFlopModule {
    fn send(&mut self, _from: Option<String>, pulse: &Pulse) -> Vec<(String, Pulse)> {
        match pulse {
            Pulse::High => vec![],
            Pulse::Low => {
                let to_send = if self.is_on {
                    Pulse::Low
                } else {
                    Pulse::High
                };

                self.is_on = !self.is_on;

                self.destinations.iter()
                    .map(|d| (d.clone(), to_send))
                    .collect()
            },
        }
    }
}

struct ConjunctionModule {
    most_recent_inputs: HashMap<String, Pulse>,
    destinations: Vec<String>,
}

impl ConjunctionModule {
    fn new(possible_inputs: &[String], destinations: &[&str]) -> ConjunctionModule {
        ConjunctionModule {
            most_recent_inputs: possible_inputs.iter()
                .map(|i| (i.to_string(), Pulse::Low))
                .collect(),
            destinations: destinations.iter().map(|s| s.to_string()).collect(),
        }
    }
}

impl Module for ConjunctionModule {
    fn send(&mut self, from: Option<String>, pulse: &Pulse) -> Vec<(String, Pulse)> {
        *self.most_recent_inputs.get_mut(&from.unwrap()).unwrap() = *pulse;

        let to_send = if self.most_recent_inputs.values().all(|p| *p == Pulse::High) {
            Pulse::Low
        } else {
            Pulse::High
        };

        self.destinations.iter()
            .map(|d| (d.to_string(), to_send))
            .collect()
    }
}

pub struct Day20 {
    input: String,
}

impl Day20 {
    pub fn new(input: String) -> Day20 {
        Day20 { input }
    }

    fn parse_input(&self) -> HashMap<String, Box<dyn Module>> {
        let inputs_for_module = self.map_all_inputs_for_modules();

        self.input.lines()
            .map(|line| line.split_once(" -> ").unwrap())
            .map(|(typed_name, destinations_s)| {
                let destinations: Vec<&str> = destinations_s.split(", ").collect();
                let name = Day20::get_untyped_module_name(typed_name);
                let module: Box<dyn Module> = match typed_name {
                    "broadcaster" => Box::new(BroadcasterModule::new(&destinations)),
                    _ if typed_name.starts_with('%') => Box::new(FlipFlopModule::new(&destinations)),
                    _ if typed_name.starts_with('&') => Box::new(ConjunctionModule::new(&inputs_for_module[&name], &destinations)),
                    _ => panic!("Unknown module: {}", typed_name),
                };
                (name, module)
            })
            .collect()
    }

    fn get_untyped_module_name(name: &str) -> String {
        if name.starts_with('%') || name.starts_with('&') {
            name.chars().skip(1).collect()
        } else {
            name.to_string()
        }
    }

    fn map_all_inputs_for_modules(&self) -> HashMap<String, Vec<String>> {
        let mut map: HashMap<String, Vec<String>> = HashMap::new();

        self.input.lines()
            .map(|line| line.split_once(" -> ").unwrap())
            .map(|(typed_name, destinations_s)| (Day20::get_untyped_module_name(typed_name), destinations_s.split(", ").collect::<Vec<&str>>()))
            .for_each(|(name, destinations)| {
                destinations.iter().for_each(|d| {
                    map.entry(d.to_string())
                        .and_modify(|inputs| inputs.push(name.clone()))
                        .or_insert(vec![name.clone()]);
                })
            });

        map
    }

    fn count_presses_until_pulse_sent(&self, wanted_source: &str, wanted_pulse: Pulse) -> usize {
        let mut modules = self.parse_input();
        let mut presses: usize = 1;

        loop {
            let mut to_process: VecDeque<(String, Option<String>, Pulse)> = VecDeque::new();
            to_process.push_back(("broadcaster".to_string(), None, Pulse::Low));

            while let Some((module_name, from, pulse)) = to_process.pop_front() {
                let new_pulses = match modules.get_mut(&module_name) {
                    None => vec![],
                    Some(m) => m.send(from, &pulse),
                };

                for (new_module_name, pulse) in new_pulses {
                    if module_name == wanted_source && pulse == wanted_pulse {
                        return presses;
                    }

                    to_process.push_back((new_module_name, Some(module_name.clone()), pulse));
                }
            }

            presses += 1;
        }

    }
}

impl Solver for Day20 {
    fn part1(&self) -> String {
        let mut modules = self.parse_input();
        let mut low_count = 0_usize;
        let mut high_count = 0_usize;

        for _ in 0..1000 {
            let mut to_process: VecDeque<(String, Option<String>, Pulse)> = VecDeque::new();
            to_process.push_back(("broadcaster".to_string(), None, Pulse::Low));

            while let Some((module_name, from, pulse)) = to_process.pop_front() {
                match pulse {
                    Pulse::High => high_count += 1,
                    Pulse::Low => low_count += 1,
                };

                let new_pulses = match modules.get_mut(&module_name) {
                    None => vec![],
                    Some(m) => m.send(from, &pulse),
                };

                for (new_module_name, pulse) in new_pulses {
                    to_process.push_back((new_module_name, Some(module_name.clone()), pulse));
                }
            }
        }

        (low_count * high_count).to_string()
    }

    fn part2(&self) -> String {
        let inputs_to_modules = self.map_all_inputs_for_modules();
        let inputs_to_rx = &inputs_to_modules["rx"];
        let inputs_to_inputs: Vec<String> = inputs_to_rx.iter()
            .flat_map(|i| inputs_to_modules[i].clone())
            .collect();
        println!("Inputs to RX: {}", slice_to_string(inputs_to_rx));
        println!("Inputs to inputs: {}", slice_to_string(&inputs_to_inputs));

        let presses: Vec<usize> = inputs_to_inputs.iter()
            .map(|source| self.count_presses_until_pulse_sent(source, Pulse::High))
            .collect();

        lcm_n(&presses).to_string()
    }
}


fn lcm_n(nums: &[usize]) -> usize {
    nums.iter().fold(1, |acc, n| lcm(acc, *n))
}
