use std::{env, fs};

mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;

mod direction;
mod point;
mod solver;
mod util;

use crate::solver::Solver;

#[macro_use]
extern crate lazy_static;

fn main() {
    let args: Vec<String> = env::args().collect();
    let day: i32 = args.get(1).unwrap().parse().unwrap();
    let part: i32 = args.get(2).unwrap().parse().unwrap();
    let input_file_path = args.get(3).unwrap();
    let input = fs::read_to_string(input_file_path).expect("Should have been able to read input file");

    let solver: Box<dyn Solver> = match day {
        4 => Box::new(day04::Day4::new(input)),
        5 => Box::new(day05::Day5::new(input)),
        6 => Box::new(day06::Day6::new(input)),
        7 => Box::new(day07::Day7::new(input)),
        8 => Box::new(day08::Day8::new(input)),
        9 => Box::new(day09::Day9::new(input)),
        10 => Box::new(day10::Day10::new(input)),
        11 => Box::new(day11::Day11::new(input)),
        12 => Box::new(day12::Day12::new(input)),
        13 => Box::new(day13::Day13::new(input)),
        14 => Box::new(day14::Day14::new(input)),
        15 => Box::new(day15::Day15::new(input)),
        16 => Box::new(day16::Day16::new(input)),
        17 => Box::new(day17::Day17::new(input)),
        18 => Box::new(day18::Day18::new(input)),
        19 => Box::new(day19::Day19::new(input)),
        20 => Box::new(day20::Day20::new(input)),
        21 => Box::new(day21::Day21::new(input)),
        _ => panic!("Unknown day: {}", day),
    };

    let output = match part {
        1 => solver.part1(),
        2 => solver.part2(),
        _ => panic!("Unknown part: {}", part),
    };

    println!("{}", output)
}
