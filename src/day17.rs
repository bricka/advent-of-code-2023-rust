use std::{cmp::Reverse, fmt::Display, collections::HashSet};

use array2d::Array2D;
use itertools::Itertools;
use priority_queue::PriorityQueue;

use crate::{solver::Solver, point::Point, direction::Direction, util::slice_to_string};

#[derive(PartialEq, Eq, Hash, Debug)]
struct Path {
    points: Vec<Point>,
    direction: Direction,
    already_moved_in_direction: usize,
    heat_loss: usize,
}

impl Display for Path {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{{}}}", slice_to_string(&self.points))
    }
}

pub struct Day17 {
    input: String,
}

impl Day17 {
    pub fn new(input: String) -> Day17 {
        Day17 { input }
    }

    fn parse_input(&self) -> Array2D<usize> {
        Array2D::from_rows(
            &self.input.lines()
                .map(|line| line.chars().map(|c| c.to_digit(10).unwrap() as usize).collect())
                .collect::<Vec<Vec<usize>>>()
        )
        .unwrap()
    }

    fn solve(&self, map: &Array2D<usize>, minimum: usize, maximum: usize) -> Path {
        let end_point = Point::new(map.num_columns() - 1, map.num_rows() - 1);

        let mut visited: HashSet<(Point, Direction, usize)> = HashSet::new();

        let mut pq: PriorityQueue<Path, Reverse<usize>> = PriorityQueue::new();
        pq.push(Path {
            points: vec![Point::new(0, 0), Point::new(1, 0)],
            direction: Direction::Right,
            already_moved_in_direction: 1,
            heat_loss: map[(0, 1)],
        }, Reverse(map[(0, 1)]));
        pq.push(Path {
            points: vec![Point::new(0, 0), Point::new(0, 1)],
            direction: Direction::Down,
            already_moved_in_direction: 1,
            heat_loss: map[(1, 0)],
        }, Reverse(map[(1, 0)]));

        while let Some((path, _)) = pq.pop() {
            let can_stop = path.already_moved_in_direction >= minimum;

            let last_point = path.points.last().unwrap();
            // println!("Path: {}, Heat Loss: {}", path, path.heat_loss);
            if can_stop && *last_point == end_point {
                return path;
            }

            let visited_key = (last_point.clone(), path.direction, path.already_moved_in_direction);

            if can_stop && visited.contains(&visited_key) {
                continue
            }

            if can_stop {
                visited.insert(visited_key);
            }

            let mut next_points: Vec<Option<(Point, Direction, usize)>> = vec![];

            if path.already_moved_in_direction >= minimum {
                let left_dir = path.direction.turn_left();
                let right_dir = path.direction.turn_right();

                next_points.push(last_point.in_direction(left_dir).map(|p| (p, left_dir, 1)));
                next_points.push(last_point.in_direction(right_dir).map(|p| (p, right_dir, 1)));
            }

            if path.already_moved_in_direction < maximum {
                if let Some(p) = last_point.in_direction(path.direction) {
                    next_points.push(Some((p, path.direction, path.already_moved_in_direction + 1)));
                }
            }

            for (p, new_dir, new_already_moved) in next_points.iter().flatten() {
                if visited.contains(&(p.clone(), *new_dir, *new_already_moved)) {
                    continue
                }

                if let Some(heat_loss) = map.get(p.y, p.x) {
                    let new_heat_loss = path.heat_loss + heat_loss;
                    // Valid coordinate
                    let mut new_points = path.points.clone();
                    new_points.push(p.clone());

                    pq.push(Path {
                        points: new_points,
                        direction: *new_dir,
                        already_moved_in_direction: *new_already_moved,
                        heat_loss: new_heat_loss,
                    }, Reverse(new_heat_loss));
                }
            }
        }

        unreachable!()
    }
}

impl Solver for Day17 {
    fn part1(&self) -> String {
        let map = self.parse_input();
        self.solve(&map, 0, 3).heat_loss.to_string()
    }

    fn part2(&self) -> String {
        let map = self.parse_input();
        let path = self.solve(&map, 4, 10);
        println!("{}", display_path(&map, &path));

        path.heat_loss.to_string()
    }
}

fn display_path(map: &Array2D<usize>, path: &Path) -> String {
    let path_coords: HashSet<(usize, usize)> = path.points.iter()
        .map(|p| (p.y, p.x))
        .collect();

    map.rows_iter()
        .enumerate()
        .map(|(y, row)| row
             .enumerate()
             .map(|(x, &h)| if path_coords.contains(&(y, x)) {
                 '*'
             } else {
                 char::from_digit(h as u32, 10).unwrap()
             })
             .collect::<String>()
        )
        .join("\n")
}
