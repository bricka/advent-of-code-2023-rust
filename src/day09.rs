use std::fmt::Display;

use crate::solver::Solver;

struct Sequence {
    history: Vec<i64>,
}

impl Sequence {
    fn new(history: Vec<i64>) -> Sequence {
        Sequence {
            history,
        }
    }

    fn next(&self) -> i64 {
        let diff = self.calculate_difference_seq();

        if !self.history.iter().any(|&n| n != 0) {
            return 0
        }

        self.history.last().unwrap() + diff.next()
    }

    fn prev(&self) -> i64 {
        let diff = self.calculate_difference_seq();

        if !self.history.iter().any(|&n| n != 0) {
            return 0
        }

        self.history.first().unwrap() - diff.prev()
    }

    fn calculate_difference_seq(&self) -> Sequence {
        Sequence::new(self.history.windows(2).map(|window| window[1] - window[0]).collect())
    }
}

impl Display for Sequence {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = self.history.iter()
               .map(|n| n.to_string())
               .collect::<Vec<String>>()
               .join(" ");
        write!(f, "{}", s)
    }
}

pub struct Day9 {
    input: String,
}

impl Day9 {
    pub fn new(input: String) -> Day9 {
        Day9 { input }
    }

    fn parse_input(&self) -> Vec<Sequence> {
        self.input.lines()
            .map(|line| Sequence::new(line.split_whitespace()
                                      .map(|n| n.parse::<i64>().unwrap())
                                      .collect())
            )
            .collect()
    }
}

impl Solver for Day9 {
    fn part1(&self) -> String {
        self.parse_input()
            .iter()
            .map(|s| s.next())
            .sum::<i64>()
            .to_string()
    }

    fn part2(&self) -> String {
        self.parse_input()
            .iter()
            .map(|s| {
                let p = s.prev();
                // println!("Prev of {} is {}", s, p);
                p
            })
            .sum::<i64>()
            .to_string()
    }
}
