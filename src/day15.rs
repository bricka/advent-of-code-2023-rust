use std::{collections::HashMap, fmt::Display};

use regex::Regex;

use crate::solver::Solver;

lazy_static! {
    static ref REMOVE_REGEX: Regex = Regex::new(r"^([a-z]+)-$").expect("Regex should be valid");
    static ref SET_REGEX: Regex = Regex::new(r"^([a-z]+)=(\d+)$").expect("Regex should be valid");
}

#[derive(Clone)]
struct Lens {
    label: String,
    focal_length: usize,
}

impl Display for Lens {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{} {}]", self.label, self.focal_length)
    }
}

enum Command {
    Set(Lens),
    Remove(String),
}

impl Command {
    fn new(text: &str) -> Command {
        if let Some((_, [label])) = REMOVE_REGEX.captures(text).map(|c| c.extract()) {
            Command::Remove(label.to_string())
        } else if let Some((_, [label, focal_length])) = SET_REGEX.captures(text).map(|c| c.extract()) {
            Command::Set(Lens {
                label: label.to_string(),
                focal_length: focal_length.parse().unwrap(),
            })
        } else {
            panic!("Invalid step: {}", text)
        }
    }

    fn get_box(&self) -> usize {
        match self {
            Command::Remove(label) => hash(label),
            Command::Set(l) => hash(&l.label),
        }
    }
}

pub struct Day15 {
    input: String,
}

impl Day15 {
    pub fn new(input: String) -> Day15 {
        Day15 { input }
    }
}

fn hash(s: &str) -> usize {
    s.chars().fold(0, |acc, c| ((acc + (c as usize)) * 17) % 256)
}

impl Solver for Day15 {
    fn part1(&self) -> String {
        self.input
            .trim()
            .split(',')
            .map(|step| {
                // let v = hash(step);
                // println!("{} becomes {}", step, v);
                // v
                hash(step)
            })
            .sum::<usize>()
            .to_string()
    }

    fn part2(&self) -> String {
        let mut boxes: HashMap<usize, Vec<Lens>> = HashMap::new();
        self.input
            .trim()
            .split(',')
            .map(Command::new)
            .for_each(|command| match &command {
                Command::Remove(label) => if let Some(lenses) = boxes.get_mut(&command.get_box()) {
                    if let Some(index) = lenses.iter().position(|l| l.label == *label) {
                        lenses.remove(index);
                    }
                },
                Command::Set(lens) => {
                    boxes.entry(command.get_box())
                        .and_modify(|lenses| {
                            if let Some(index) = lenses.iter().position(|l| l.label == lens.label) {
                                lenses[index] = lens.clone();
                            } else {
                                lenses.push(lens.clone());
                            }
                        })
                        .or_insert(vec![lens.clone()]);
                }
            });

        // Output boxes
        // boxes.iter()
        //     .sorted_by_key(|(&n, _)| n)
        //     .for_each(|(n, lenses)| {
        //         if !lenses.is_empty() {
        //             println!("Box {}: {}", n, lenses.iter().map(|l| l.to_string()).join(" "));
        //         }
        //     });

        boxes.iter()
            .map(|(box_number, lenses)|
                 lenses.iter()
                 .enumerate()
                 .map(|(slot, lens)| (box_number + 1) * (slot + 1) * lens.focal_length)
                .sum::<usize>()
            )
            .sum::<usize>()
            .to_string()
    }
}
