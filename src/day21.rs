use std::collections::HashSet;

use array2d::Array2D;

use crate::{solver::Solver, point::Point};

const STEP_GOAL: usize = 64;

#[derive(PartialEq, Eq, Clone, Copy)]
enum Tile {
    Rock,
    Garden,
    Start,
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            'S' => Tile::Start,
            '.' => Tile::Garden,
            '#' => Tile::Rock,
            _ => panic!("Invalid tile char: {}", value),
        }
    }
}

pub struct Day21 {
    input: String,
}

impl Day21 {
    pub fn new(input: String) -> Day21 {
        Day21 { input }
    }

    fn parse_input(&self) -> Array2D<Tile> {
        let rows: Vec<Vec<Tile>> = self.input.lines()
            .map(|row| row.chars().map(Tile::from).collect())
            .collect();

        Array2D::from_rows(&rows).unwrap()
    }
}

impl Solver for Day21 {
    fn part1(&self) -> String {
        let grid = self.parse_input();
        let starting_coord = find_start(&grid);

        let mut reachable: HashSet<Point> = HashSet::new();
        let mut already_processed: HashSet<(Point, usize)> = HashSet::new();
        let mut to_process: Vec<(Point, usize)> = vec![(starting_coord, 0)];

        while let Some((point, steps_so_far)) = to_process.pop() {
            if steps_so_far == STEP_GOAL {
                reachable.insert(point);
                continue;
            }

            if !already_processed.insert((point.clone(), steps_so_far)) {
                continue
            }

            point.neighbors()
                .iter()
                .for_each(|neighbor| {
                    match grid.get(neighbor.y, neighbor.x) {
                        None => (),
                        Some(Tile::Rock) => (),
                        Some(_) => {
                            to_process.push((neighbor.clone(), steps_so_far + 1));
                        }
                    }
                })
        }

        reachable.len().to_string()
    }

    fn part2(&self) -> String {
        todo!()
    }
}

fn find_start(grid: &Array2D<Tile>) -> Point {
    for (y, row) in grid.rows_iter().enumerate() {
        for (x, tile) in row.enumerate() {
            if *tile == Tile::Start {
                return Point::new(x, y);
            }
        }
    }

    unreachable!()
}
