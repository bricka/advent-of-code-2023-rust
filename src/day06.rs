use std::iter::zip;

use crate::day05::Solver;

struct RaceOption {
    distance: usize,
}

#[derive(Debug)]
struct Race {
    time: usize,
    distance: usize,
}

impl Race {
    fn options(&self) -> Vec<RaceOption> {
        (0..self.time).map(|hold_time| {
            let travel_time = self.time - hold_time;
            RaceOption {
                distance: hold_time * travel_time,
            }
        }).collect()
    }
}

pub struct Day6 {
    input: String,
}

fn parse_input_line(line: &str) -> Vec<usize> {
    line.split_whitespace().skip(1).map(|n| n.parse().unwrap()).collect()
}

impl Day6 {
    pub fn new(input: String) -> Day6 {
        Day6 { input }
    }

    fn part_1_parse_input(&self) -> Vec<Race> {
        let lines = self.input.lines().collect::<Vec<&str>>();
        let times = parse_input_line(lines[0]);
        let distances = parse_input_line(lines[1]);

        zip(times, distances).map(|(time, distance)| Race { time, distance }).collect()
    }

    fn part_2_parse_input(&self) -> Race {
        let lines = self.input.lines().collect::<Vec<&str>>();
        let time = parse_input_line(lines[0]).iter().fold("".to_string(), |acc, next| acc + &next.to_string()).parse::<usize>().unwrap();
        let distance = parse_input_line(lines[1]).iter().fold("".to_string(), |acc, next| acc + &next.to_string()).parse::<usize>().unwrap();

        Race { time, distance }
    }
}

impl Solver for Day6 {
    fn part1(&self) -> String {
        let input = self.part_1_parse_input();

        input.iter()
            .map(|race| {
                let options = race.options();
                let options_that_win = options.iter().map(|o| o.distance).filter(|d| *d > race.distance);
                options_that_win.count()
            })
            .product::<usize>()
            .to_string()
    }

    fn part2(&self) -> String {
        let input = self.part_2_parse_input();

        input.options().iter().map(|o| o.distance).filter(|d| d > &input.distance).count().to_string()
    }
}
