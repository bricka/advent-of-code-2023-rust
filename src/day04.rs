use std::cmp::min;
use std::collections::{HashSet, HashMap};

pub use crate::solver::Solver;

#[derive(Debug)]
struct ScratchCard {
    id: usize,
    winning_numbers: HashSet<i64>,
    my_numbers: HashSet<i64>,
}

impl ScratchCard {
    fn get_match_count(&self) -> usize {
        let intersection: HashSet<&i64> = self.winning_numbers.intersection(&self.my_numbers).collect();
        intersection.len()
    }

    fn calculate_score(&self) -> usize {
        let num_matches = self.get_match_count();
        match num_matches {
            0 => 0,
            _ => {
                let pow: u32 = u32::try_from(num_matches).unwrap() - 1;
                2_usize.checked_pow(pow).unwrap()
            }
        }
    }
}

pub struct Day4 {
    input: String,
}

fn parse_numbers(numbers: &str) -> HashSet<i64> {
    let mut set: HashSet<i64> = HashSet::new();

    numbers.split_whitespace().for_each(|n| {
        set.insert(n.parse().unwrap());
    });

    set
}

fn parse_card_id(s: &str) -> usize {
    s.split_whitespace().collect::<Vec<&str>>()[1].parse().unwrap()
}

impl Day4 {
    pub fn new(input: String) -> Day4 {
        Day4 {
            input,
        }
    }

    fn parse_cards(&self) -> Vec<ScratchCard> {
        self.input.lines()
            .map(|line| {
                let id_and_nums: Vec<&str> = line.split(": ").collect();
                let winning_and_my_nums: Vec<&str> = id_and_nums.get(1).unwrap().split(" | ").collect();
                let winning_numbers = parse_numbers(winning_and_my_nums[0]);
                let my_numbers = parse_numbers(winning_and_my_nums[1]);
                ScratchCard {
                    id: parse_card_id(id_and_nums[0]),
                    winning_numbers,
                    my_numbers,
                }
            })
            .collect()
    }
}

impl Solver for Day4 {
    fn part1(&self) -> String {
        let cards = self.parse_cards();
        let points = cards.iter().fold(0, |acc, card| acc + card.calculate_score());
        points.to_string()
    }

    fn part2(&self) -> String {
        let cards = self.parse_cards();
        let mut card_count: HashMap<usize, usize> = HashMap::new();
        let mut id_to_match_count: HashMap<usize, usize> = HashMap::new();
        cards.iter().for_each(|c| {
            card_count.insert(c.id, 1);
            id_to_match_count.insert(c.id, c.get_match_count());
        });

        let max_card = cards.iter().map(|c| c.id).max().unwrap();

        for card_id in 1..=max_card {
            let count = *card_count.get(&card_id).unwrap();
            let score = *id_to_match_count.get(&card_id).unwrap();

            if score > 0 {
                let max_new_card = min(card_id+score, max_card);
                for new_card in (card_id+1)..=(max_new_card) {
                    let new_count = card_count.get(&new_card).map(|c| c + count).unwrap_or(count);
                    card_count.insert(new_card, new_count);
                }
            }
        }

        card_count.values().sum::<usize>().to_string()
    }
}
