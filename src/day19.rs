use std::{collections::HashMap, cmp::Ordering, fmt::{Display, Debug}, ops::Range};

use regex::Regex;
use strum::EnumIter;
use strum::IntoEnumIterator;

use crate::solver::Solver;

lazy_static! {
    static ref WORKFLOW_REGEX: Regex = Regex::new(r"^(\w+)\{(\w[<>]\d+:\w+(?:,\w[<>]\d+:\w+)*)?,(\w+)\}$").expect("Regex should be valid");
    static ref PART_REGEX: Regex = Regex::new(r"^\{(.*)\}$").expect("Regex should be valid");
}

#[derive(PartialEq, Eq, Clone, Copy, Hash, EnumIter)]
enum PartProperty {
    CoolLooking,
    Musical,
    Aerodynamic,
    Shiny,
}

impl From<char> for PartProperty {
    fn from(value: char) -> Self {
        match value {
            'x' => PartProperty::CoolLooking,
            'm' => PartProperty::Musical,
            'a' => PartProperty::Aerodynamic,
            's' => PartProperty::Shiny,
            _ => panic!("Unknown property: {}", value),
        }
    }
}

impl From<&str> for PartProperty {
    fn from(value: &str) -> Self {
        match value {
            "x" => PartProperty::CoolLooking,
            "m" => PartProperty::Musical,
            "a" => PartProperty::Aerodynamic,
            "s" => PartProperty::Shiny,
            _ => panic!("Unknown property: {}", value),
        }
    }
}

impl Display for PartProperty {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            PartProperty::CoolLooking => 'x',
            PartProperty::Musical => 'm',
            PartProperty::Aerodynamic => 'a',
            PartProperty::Shiny => 's',
        })
    }
}

struct Part {
    properties: HashMap<PartProperty, usize>,
}

impl Part {
    fn total_score(&self) -> usize {
        self.properties.values().sum()
    }
}

#[derive(Clone)]
enum WorkflowResult {
    Approved,
    Rejected,
    NewWorkflow(String),
}

impl From<&str> for WorkflowResult {
    fn from(value: &str) -> Self {
        match value {
            "A" => WorkflowResult::Approved,
            "R" => WorkflowResult::Rejected,
            _ => WorkflowResult::NewWorkflow(value.to_string()),
        }
    }
}

impl Display for WorkflowResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            WorkflowResult::Approved => "A",
            WorkflowResult::Rejected => "R",
            WorkflowResult::NewWorkflow(s) => s,
        })
    }
}

impl Debug for WorkflowResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

#[derive(Clone, Copy)]
enum WorkflowComparator {
    LT,
    GT,
}

impl From<char> for WorkflowComparator {
    fn from(value: char) -> Self {
        match value {
            '<' => WorkflowComparator::LT,
            '>' => WorkflowComparator::GT,
            _ => panic!("Unknown comparator: {}", value),
        }
    }
}

impl Display for WorkflowComparator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            WorkflowComparator::LT => '<',
            WorkflowComparator::GT => '>',
        })
    }
}

impl Debug for WorkflowComparator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

struct WorkflowStep {
    property: PartProperty,
    comparator: WorkflowComparator,
    threshold: usize,
    result: WorkflowResult,
}

impl WorkflowStep {
    fn apply(&self, part: &Part) -> Option<WorkflowResult> {
        match (part.properties[&self.property].cmp(&self.threshold), self.comparator) {
            (Ordering::Greater, WorkflowComparator::GT) => Some(self.result.clone()),
            (Ordering::Less, WorkflowComparator::LT) => Some(self.result.clone()),
            _ => None,
        }
    }

    fn carve_range(&self, range: &XmasRange) -> Vec<(XmasRange, Option<WorkflowResult>)> {
        let (matched, remaining) = range.split(self.property, self.comparator, self.threshold);

        [
            matched.map(|r| (r, Some(self.result.clone()))),
            remaining.map(|r| (r, None))
        ]
            .iter()
            .flatten()
            .cloned()
            .collect()
    }
}

impl Display for WorkflowStep {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}{}:{}", self.property, self.comparator, self.threshold, self.result)
    }
}

impl Debug for WorkflowStep {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

#[derive(Debug)]
struct Workflow {
    steps: Vec<WorkflowStep>,
    default: WorkflowResult,
}

impl Workflow {
    fn apply(&self, part: &Part) -> WorkflowResult {
        self.steps.iter()
            .find_map(|s| s.apply(part))
            .unwrap_or(self.default.clone())
    }

    fn carve_range(&self, range: &XmasRange) -> Vec<(XmasRange, WorkflowResult)> {
        let mut results = vec![];
        let mut remaining_ranges: Vec<XmasRange> = vec![range.clone()];

        for step in &self.steps {
            let ranges = remaining_ranges.clone();
            remaining_ranges.clear();

            for range in ranges {
                for (range, result) in step.carve_range(&range) {
                    match result {
                        None => remaining_ranges.push(range),
                        Some(r) => results.push((range, r)),
                    }
                }
            }
        }

        if !remaining_ranges.is_empty() {
            let mut remaining_results = remaining_ranges.iter()
                    .map(|r| (r.clone(), self.default.clone()))
                    .collect();
            results.append(&mut remaining_results);
        }

        results
    }
}

pub struct Day19 {
    input: String,
}

fn parse_workflow_line(line: &str) -> (String, Workflow) {
    let captures = WORKFLOW_REGEX.captures(line).unwrap();

    let steps = captures.get(2).map(|steps_str| {
        steps_str.as_str()
            .split(',')
            .map(|step_str| {
                let property = PartProperty::from(step_str.chars().next().unwrap());
                let comparator = WorkflowComparator::from(step_str.chars().nth(1).unwrap());
                let threshold = step_str.chars()
                    .skip(2)
                    .take_while(|&c| c != ':')
                    .collect::<String>()
                    .parse()
                    .unwrap();
                let result = WorkflowResult::from(
                    step_str.chars()
                        .skip_while(|&c| c != ':')
                        .skip(1)
                        .collect::<String>()
                        .as_str()
                );

                WorkflowStep {
                    property,
                    comparator,
                    threshold,
                    result,
                }
            })
            .collect()
    })
    .unwrap_or(vec![]);

    (captures.get(1).unwrap().as_str().to_string(), Workflow {
        steps,
        default: WorkflowResult::from(captures.get(3).unwrap().as_str()),
    })
}

fn parse_part_line(line: &str) -> Part {
    let (_, [properties]) = PART_REGEX.captures(line).unwrap().extract();

    Part {
        properties: properties.split(',')
            .map(|p| p.split_once('=').unwrap())
            .map(|(p, v)| (PartProperty::from(p), v.parse().unwrap()))
            .collect()
    }
}

impl Day19 {
    pub fn new(input: String) -> Day19 {
        Day19 { input }
    }

    fn parse_input(&self) -> (HashMap<String, Workflow>, Vec<Part>) {
        let lines: Vec<String> = self.input.lines().map(|s| s.to_string()).collect();
        let input_parts: Vec<&[String]> = lines
            .split(|line| line.is_empty())
            .collect();

        let workflows = input_parts[0].iter()
            .map(|line| parse_workflow_line(line))
            .collect();

        let parts = input_parts[1].iter()
            .map(|line| parse_part_line(line))
            .collect();

        (workflows, parts)
    }
}

fn is_part_approved(part: &Part, current_workflow: &Workflow, workflows: &HashMap<String, Workflow>) -> bool {
    match current_workflow.apply(part) {
        WorkflowResult::Approved => true,
        WorkflowResult::Rejected => false,
        WorkflowResult::NewWorkflow(w) => is_part_approved(part, &workflows[&w], workflows),
    }
}

#[derive(Clone)]
struct XmasRange {
    ranges: HashMap<PartProperty, Range<usize>>,
}

impl XmasRange {
    fn new(minimum: usize, maximum: usize) -> XmasRange {
        let range = Range {
            start: minimum,
            end: maximum + 1,
        };

        XmasRange {
            ranges: PartProperty::iter()
                .map(|p| (p, range.clone()))
                .collect()
        }
    }

    fn len(&self) -> usize {
        self.ranges
            .values()
            .map(|r| r.len())
            .product()
    }

    fn split(&self, property: PartProperty, comparator: WorkflowComparator, threshold: usize) -> (Option<XmasRange>, Option<XmasRange>) {
        let range = &self.ranges[&property];
        match comparator {
            WorkflowComparator::LT => if range.start >= threshold {
                (None, Some(self.clone()))
            } else if range.end <= threshold {
                (Some(self.clone()), None)
            } else {
                (
                    Some(self.with_property_to(property, threshold-1)),
                    Some(self.with_property_from(property, threshold)),
                )
            },
            WorkflowComparator::GT => if range.end <= threshold {
                (None, Some(self.clone()))
            } else if range.start >= threshold {
                (Some(self.clone()), None)
            } else {
                (
                    Some(self.with_property_from(property, threshold+1)),
                    Some(self.with_property_to(property, threshold)),
                )
            }
        }
    }

    fn with_property_to(&self, property: PartProperty, to: usize) -> XmasRange {
        let mut new_map = self.ranges.clone();
        let existing_range = &new_map[&property];
        let new_range = Range {
            start: existing_range.start,
            end: to + 1,
        };
        new_map.insert(property, new_range);

        XmasRange { ranges: new_map }
    }

    fn with_property_from(&self, property: PartProperty, from: usize) -> XmasRange {
        let mut new_map = self.ranges.clone();
        let existing_range = &new_map[&property];
        let new_range = Range {
            start: from,
            end: existing_range.end,
        };
        new_map.insert(property, new_range);

        XmasRange { ranges: new_map }
    }
}

fn find_valid_ranges(initial_range: XmasRange, workflows: &HashMap<String, Workflow>) -> Vec<XmasRange> {
    let mut to_check = vec![("in".to_string(), initial_range)];
    let mut approved_ranges: Vec<XmasRange> = vec![];

    while let Some((workflow_name, range)) = to_check.pop() {
        let workflow = &workflows[&workflow_name];
        for result in workflow.carve_range(&range) {
            match result {
                (r, WorkflowResult::Approved) => {
                    approved_ranges.push(r)
                },
                (r, WorkflowResult::NewWorkflow(new_workflow)) => {
                    to_check.push((new_workflow, r))
                }
                (_, WorkflowResult::Rejected) => (),
            }
        }
    }

    approved_ranges
}

impl Solver for Day19 {
    fn part1(&self) -> String {
        let (workflows, parts) = self.parse_input();
        let start_workflow = &workflows["in"];

        parts.iter()
            .filter(|p| is_part_approved(p, start_workflow, &workflows))
            .map(|p| p.total_score())
            .sum::<usize>()
            .to_string()

    }

    fn part2(&self) -> String {
        let (workflows, _) = self.parse_input();

        find_valid_ranges(XmasRange::new(1, 4000), &workflows)
            .iter()
            .map(|r| r.len())
            .sum::<usize>()
            .to_string()
    }
}
