use std::collections::HashMap;

use crate::point::Point;

pub fn slice_to_string<T: ToString>(v: &[T]) -> String {
    v.iter().map(|it| it.to_string()).collect::<Vec<String>>().join(", ")
}

pub fn pointmap_to_string_grid<T: ToString>(hm: &HashMap<Point, T>, default: &str) -> String {
    let max_x = hm.keys().map(|p| p.x).max().unwrap();
    let max_y = hm.keys().map(|p| p.y).max().unwrap();

    let mut out: Vec<String> = vec![];

    for y in 0..=max_y {
        let mut line = String::new();
        for x in 0..=max_x {
            line += &hm.get(&Point { x, y }).map(|v| v.to_string()).unwrap_or(default.to_string())
        }

        out.push(line);
    }

    out.join("\n")
}
