use std::{str::Lines, iter::zip, fmt::Display};

use crate::solver::Solver;

struct InputIterator<'a> {
    lines: Lines<'a>,
}

impl<'a> InputIterator<'a> {
    fn new(raw_input: &'a str) -> InputIterator {
        InputIterator {
            lines: raw_input.lines(),
        }
    }
}

impl<'a> Iterator for InputIterator<'a> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.lines.next().map(|first| {
            let mut lines = vec![first];
            for next in self.lines.by_ref() {
                if next.is_empty() {
                    break
                }

                lines.push(next);
            }

            lines.join("\n")
        })
    }
}

#[derive(PartialEq, Copy, Clone)]
enum Ground {
    Ash,
    Rocks,
}

impl From<char> for Ground {
    fn from(value: char) -> Self {
        match value {
            '.' => Ground::Ash,
            '#' => Ground::Rocks,
            _ => panic!("Unknown ground char: {}", value),
        }
    }
}

impl Display for Ground {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Ground::Ash => '.',
            Ground::Rocks => '#',
        })
    }
}

fn line_diff(line1: &[Ground], line2: &[Ground]) -> usize {
    zip(line1, line2).filter(|(&a, &b)| a != b).count()
}

fn do_all_reflect(remaining_above_lines: &[Vec<Ground>], remaining_below_lines: &[Vec<Ground>], already_fixed_smudge: bool) -> bool {
    if remaining_above_lines.is_empty() || remaining_below_lines.is_empty() {
        return already_fixed_smudge;
    }

    let (first_above, remaining_above) = remaining_above_lines.split_first().unwrap();
    let (first_below, remaining_below) = remaining_below_lines.split_first().unwrap();

    // println!("Comparing above {} with below {}", slice_to_string(first_above), slice_to_string(first_below));

    let line_diff = line_diff(first_above, first_below);
    match (line_diff, already_fixed_smudge) {
        (0, _) => do_all_reflect(remaining_above, remaining_below, already_fixed_smudge),
        (1, false) => {
            // println!("Correcting smudge");
            do_all_reflect(remaining_above, remaining_below, true)
        },
        (_, _) => false,
    }
}

fn score_rows(rows: &[Vec<Ground>], factor: usize, allow_smudges: bool) -> usize {
    for i in 1..rows.len() {
        println!("Splitting at {}", i);
        let (raw_above, below) = rows.split_at(i);
        let above = raw_above.iter().rev().cloned().collect::<Vec<Vec<Ground>>>();

        let correct_split = if allow_smudges {
            do_all_reflect(&above, below, false)
        } else {
            zip(above, below).all(|(above_row, below_row)| above_row == *below_row)
        };

        if correct_split {
            println!("Line between {} and {}", i, i+1);
            return i * factor;
        }
    }

    0
}

struct Pattern {
    grid: Vec<Vec<Ground>>,
}

impl Pattern {
    fn new(input: &str) -> Pattern {
        Pattern {
            grid: input.lines()
                .map(|line| line.chars().map(Ground::from).collect())
                .collect()
        }
    }

    fn score_cols(&self, allow_smudges: bool) -> usize {
        score_rows(&self.rows_to_cols(), 1, allow_smudges)
    }

    fn score_rows(&self, allow_smudges: bool) -> usize {
        score_rows(&self.grid, 100, allow_smudges)
    }

    fn rows_to_cols(&self) -> Vec<Vec<Ground>> {
        (0..self.grid[0].len())
            .map(|y| (0..self.grid.len()).map(|x| self.grid[x][y]).collect())
            .collect()
    }
}

pub struct Day13 {
    input: String,
}

impl Day13 {
    pub fn new(input: String) -> Day13 {
        Day13 { input }
    }
}

impl Solver for Day13 {
    fn part1(&self) -> String {
        let inputs = InputIterator::new(&self.input);
        inputs
            .map(|i| {
                println!("Input:\n{}", i);
                i
            })
            .map(|i| Pattern::new(&i))
            .map(|p| p.score_cols(false) + p.score_rows(false))
            .map(|score| {
                println!("Score is {}", score);
                score
            })
            .sum::<usize>()
            .to_string()
    }

    fn part2(&self) -> String {
        let inputs = InputIterator::new(&self.input);
        inputs
            .map(|i| {
                println!("Input:\n{}", i);
                i
            })
            .map(|i| Pattern::new(&i))
            .map(|p| p.score_cols(true) + p.score_rows(true))
            .map(|score| {
                println!("Score is {}", score);
                score
            })
            .sum::<usize>()
            .to_string()
    }
}
