use std::fmt::{Debug, Display};
use strum::IntoEnumIterator;

use crate::direction::Direction;

#[derive(PartialEq, Eq, Hash, Clone)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    pub fn new(x: usize, y: usize) -> Point {
        Point { x, y }
    }

    pub fn up(&self) -> Option<Point> {
        match self.y {
            0 => None,
            _ => Some(Point { x: self.x, y: self.y - 1 }),
        }
    }

    pub fn down(&self) -> Point {
        Point { x: self.x, y: self.y + 1 }
    }

    pub fn left(&self) -> Option<Point> {
        match self.x {
            0 => None,
            _ => Some(Point { x: self.x - 1, y: self.y }),
        }
    }

    pub fn right(&self) -> Point {
        Point { x: self.x + 1, y: self.y }
    }

    pub fn in_direction(&self, dir: Direction) -> Option<Point> {
        match dir {
            Direction::Left => self.left(),
            Direction::Right => Some(self.right()),
            Direction::Up => self.up(),
            Direction::Down => Some(self.down()),
        }
    }

    pub fn neighbors(&self) -> Vec<Point> {
        Direction::iter()
            .map(|d| self.in_direction(d))
            .flatten()
            .collect()
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl Debug for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}
