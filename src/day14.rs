use std::{fmt::Display, collections::HashMap, cmp::Reverse, hash::Hash};

use itertools::Itertools;

use crate::{solver::Solver, util::pointmap_to_string_grid, point::Point};

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
enum Rock {
    Rounded,
    Cube,
    // None,
}

impl Display for Rock {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Rock::Rounded => 'O',
            Rock::Cube => '#',
            // Rock::None => '.',
        })
    }
}

impl From<char> for Rock {
    fn from(value: char) -> Self {
        match value {
            'O' => Rock::Rounded,
            '#' => Rock::Cube,
            // '.' => Rock::None,
            _ => panic!("Unknown rock: {}", value),
        }
    }
}

fn find_new_pos_north_of(p: &Point, hm: &HashMap<Point, Rock>, _max_x: usize, _max_y: usize) -> Point {
    if p.y == 0 {
        return p.clone()
    }

    let new_y = hm.keys()
        .filter(|other| other.x == p.x)
        .map(|other| other.y)
        .filter(|&y| y < p.y)
        .max()
        .map(|y| y + 1)
        .unwrap_or(0);
    Point { x: p.x, y: new_y }
}

fn find_new_pos_west_of(p: &Point, hm: &HashMap<Point, Rock>, _max_x: usize, _max_y: usize) -> Point {
    if p.x == 0 {
        return p.clone()
    }

    let new_x = hm.keys()
        .filter(|other| other.y == p.y)
        .map(|other| other.x)
        .filter(|&x| x < p.x)
        .max()
        .map(|x| x + 1)
        .unwrap_or(0);
    Point { x: new_x, y: p.y }
}

fn find_new_pos_south_of(p: &Point, hm: &HashMap<Point, Rock>, _max_x: usize, max_y: usize) -> Point {
    if p.y == max_y {
        return p.clone();
    }

    let new_y = hm.keys()
        .filter(|other| other.x == p.x)
        .map(|other| other.y)
        .filter(|&y| y > p.y)
        .min()
        .map(|y| y - 1)
        .unwrap_or(max_y);

    Point { x: p.x, y: new_y }
}

fn find_new_pos_east_of(p: &Point, hm: &HashMap<Point, Rock>, max_x: usize, _max_y: usize) -> Point {
    if p.x == max_x {
        return p.clone()
    }

    let new_x = hm.keys()
        .filter(|other| other.y == p.y)
        .map(|other| other.x)
        .filter(|&x| x > p.x)
        .min()
        .map(|x| x - 1)
        .unwrap_or(max_x);
    Point { x: new_x, y: p.y }
}

#[derive(PartialEq, Eq, Clone)]
struct Platform {
    rock_pos: HashMap<Point, Rock>,
    max_x: usize,
    max_y: usize,
}

impl Platform {
    fn tilt_north(&self) -> Platform {
        self.tilt(
            |(p, _)| p.y,
            |p, _, _| p.y == 0,
            find_new_pos_north_of
        )
    }

    fn tilt_west(&self) -> Platform {
        self.tilt(
            |(p, _)| p.x,
            |p, _, _| p.x == 0,
            find_new_pos_west_of
        )
    }

    fn tilt_south(&self) -> Platform {
        self.tilt(
            |(p, _)| Reverse(p.y),
            |p, _, max_y| p.y == max_y,
            find_new_pos_south_of
        )
    }

    fn tilt_east(&self) -> Platform {
        self.tilt(
            |(p, _)| Reverse(p.x),
            |p, max_x, _| p.x == max_x,
            find_new_pos_east_of
        )
    }

    fn tilt<F, S: Ord>(
        &self,
        sort_points_function: F,
        at_edge_function: fn(&Point, usize, usize) -> bool,
        new_pos_function: fn(&Point, &HashMap<Point, Rock>, usize, usize) -> Point
    ) -> Platform
    where F: FnMut(&(&Point, &Rock)) -> S {
        let mut points: Vec<(&Point, &Rock)> = self.rock_pos.iter().collect();
        points.sort_by_key(sort_points_function);

        let mut new_positions: HashMap<Point, Rock> = HashMap::new();

        for (p, &r) in points {
            if at_edge_function(p, self.max_x, self.max_y) || r != Rock::Rounded {
                new_positions.insert(p.clone(), r);
            } else {
                let new_p = new_pos_function(p, &new_positions, self.max_x, self.max_y);
                new_positions.insert(new_p, r);
            }
        }

        Platform {
            rock_pos: new_positions,
            max_x: self.max_x,
            max_y: self.max_y,
        }
    }

    fn north_load(&self) -> usize {
        let max_y = self.rock_pos.keys().map(|p| p.y).max().unwrap();
        self.rock_pos.iter()
            .map(|(p, r)| match r {
                Rock::Cube => 0,
                Rock::Rounded => max_y - p.y + 1,
            })
            .sum()

    }
}

impl Display for Platform {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", pointmap_to_string_grid(&self.rock_pos, "."))
    }
}

impl Hash for Platform {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        // `Hasher` requires that we call `hash` in the same order every time, so we need to sort our points
        self.rock_pos.iter().sorted_by_key(|(p, _)| (p.x, p.y)).for_each(|pair| pair.hash(state));
        self.max_x.hash(state);
        self.max_y.hash(state);
    }
}

pub struct Day14 {
    input: String,
}

impl Day14 {
    pub fn new(input: String) -> Day14 {
        Day14 { input }
    }

    fn parse_input(&self) -> Platform {
        let max_y = self.input.lines().count() - 1;
        let max_x = self.input.split_once('\n').unwrap().0.len() - 1;
        Platform {
            rock_pos: self.input.lines()
                .enumerate()
                .flat_map(|(y, line)| line.chars()
                          .enumerate()
                          .filter_map(move |(x, c)| match c {
                              '#' => Some((Point { x, y }, Rock::Cube)),
                              'O' => Some((Point { x, y }, Rock::Rounded)),
                              _ => None,
                          }))
                .collect(),
            max_x,
            max_y,
        }
    }

}

fn cycle_platform_n_times(initial_platform: &Platform, n: usize) -> Platform {
    let mut platform = initial_platform.clone();
    let mut current_cycle = 0;
    while current_cycle < n {
        platform = platform.tilt_north()
            .tilt_west()
            .tilt_south()
            .tilt_east();
        current_cycle += 1;
    }

    platform
}

fn cycle_platform_until_repeat(initial_platform: &Platform, n: usize) -> Option<(Platform, usize, usize)> {
    let mut seen: HashMap<Platform, usize> = HashMap::new();
    let mut current_cycle = 0;
    let mut platform = initial_platform.clone();

    while current_cycle < n {
        seen.insert(platform.clone(), current_cycle);
        platform = platform.tilt_north();
        platform = platform.tilt_west();
        platform = platform.tilt_south();
        platform = platform.tilt_east();

        current_cycle += 1;

        if let Some(&last_seen) = seen.get(&platform) {
            return Some((platform, last_seen, current_cycle));
        }
    }

    None
}

impl Solver for Day14 {
    fn part1(&self) -> String {
        let platform = self.parse_input();
        println!("Initial:\n{}", platform);

        let tilted = platform.tilt_north();
        println!("After tilting north:\n{}", tilted);

        tilted.north_load().to_string()
    }

    fn part2(&self) -> String {
        let platform = self.parse_input();
        let cycles = 1_000_000_000;

        let (_, first_occurrence, next_seen) = cycle_platform_until_repeat(&platform, cycles).unwrap();

        let after_first = cycles - first_occurrence;
        let length_of_repeated_cycle = next_seen - first_occurrence;
        let necessary_tilts = first_occurrence + ((cycles - first_occurrence) % length_of_repeated_cycle);
        println!("After First: {}, Repeated: {}, Tilts: {}", after_first, length_of_repeated_cycle, necessary_tilts);

        let final_state = cycle_platform_n_times(&platform, necessary_tilts);

        final_state.north_load().to_string()
    }
}
