pub trait Solver {
    fn part1(&self) -> String;
    fn part2(&self) -> String;
}
